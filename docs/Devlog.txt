ABYSS (Working Title) DEVELOPERS LOGS
===============================

PRE 7DFPS
----------
9th August, 00:05

Decided to spend the two days before the jam starts preparing my engine. This mostly involves bug-fixing, 
and implementing a new level editor.
So far, the editor is Screen based.
Speaking of Screens, that's a new concept I added to the engine to speed development along.
Screens are simple classes that contain an update() method and an isDone() method. 
A list of screens is kept by the main execution loop, and if the list contains any Screens, 
execution is handed over to the top-most screen's update method.

This has provided a clean way of executing menus and such.

Now,  as I was saying, the Editor is screen based; it differs a bit in that
it too contains a list of screens to manage (So it can have it's own child screens).
The best way to think of the engine at the moment is a single-thread mini OS for games,
and the screens are processes with very simple and clearly defined entry points.

The Editor is going to use the concept of Tiles to draw level floors and ceilings (I'm going
to allow for quick switching between floor and ceiling mode, with an onionskin to help),
and walls will be separately handled (Unliked the GridMap editor).
I'm hoping to wrap up the basic editor by tomorrow. I also want to get started with some
basic player code, so I can start testing out optimal render techniques for the new level
format (And add in some basic occlusion testing).
The player's collision code is going to be better this time too; not the FUBAR mess it was
in Exile. I'll be using simple Line/Line intersection to test collisions (Just need to check any
wall sections in nearby grid cells).

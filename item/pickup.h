// Provides a simple pickup system
// --

#ifndef PICKUP_H
#define PICKUP_H

#include <mio.h>
#include <math.h>
#include <mxmath.h>
#include <iostream>
#include <fstream>

#include "../EmuStream.h"

#include "../spectrum/spectrum.h"
#define GL_GLEXT_PROTOTYPES
#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glext.h>
#include "../light.h"
#include "../voxel.h"
#include "../model_obj.h"

extern bool g_switchLevel;
extern int  nxtlevel;
extern int current_map_index;

#include "../projectiles.h"

typedef struct t_pickup
{
    MXVector pos;
    int type;
    int dead;

    Sprite* sprite;

    t_pickup()
    {
        pos = MXVector(0,0,0);
        type = 0;
        sprite = NULL;
        dead = 0;
    }

    t_pickup(const t_pickup& P)
    {
        pos = P.pos;
        type = P.type;
        sprite = new Sprite(*P.sprite);
        dead = P.dead;
    }

    t_pickup(MXVector apos, int atype, Sprite* asprite)
    {
        pos = apos;
        type = atype;
        sprite = asprite;
        dead = 0;
    }

    ~t_pickup()
    {
        if(sprite)delete sprite;
    }
}Pickup;

class tPickups
{
        model_obj_t mod_mp_bottle;
        sf::Texture tex_mp_bottle;
    public:
        vector<Pickup*> ls_p;

        tPickups();

        ~tPickups();

        void Add(Pickup* p);

        void Update(MXLMap& map);

        Pickup* IsHit(MXVector p, MXLMap& m, int r = 1);

        void Render(MXVector mpv);
};


extern tPickups *g_tpickups;

#endif

#include <mgl_util.h>
#include <GL/GL.h>
#include <mxmath.h>
#include <iostream>

#include <player.h>

#include <glm/glm.hpp>

using namespace std;

void mglBillboardBegin()
{



}

void mglBillboardEnd()
{
    glPopMatrix();
}

void mglDrawTestQuad(float x, float y, float z, float r = 1.0F)
{
    Player& p = *Player::getPlayer();
    glPushMatrix();
    //glLoadIdentity();
    p.setCustomView(p.rot.x,p.rot.y+90.0F);
    float rx = p.pvx;
    float ry = p.pvy;
    p.update();

    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex3f(x-r,y+r*2,z);
        glTexCoord2f(1,0); glVertex3f(x+r,y+r*2,z);
        glTexCoord2f(1,1); glVertex3f(x+r,y,z);
        glTexCoord2f(0,1); glVertex3f(x-r,y,z);

        //glTexCoord2f(0,0); glVertex3f(-r,r*2,0);
        //glTexCoord2f(1,0); glVertex3f(r,r*2,0);
        //glTexCoord2f(1,1); glVertex3f(r,0,0);
        //glTexCoord2f(0,1); glVertex3f(-r,0,0);
    glEnd();

    glPopMatrix();
}

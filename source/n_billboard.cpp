// Billboard implementation
//

#include <SFML\Graphics.hpp>
#include <gl\gl.h>
#include <spectrum\engine\assets.h>
#include <n_billboard.h>

using namespace std;

NBillboard::NBillboard(){}

NBillboard::~NBillboard(){}

void NBillboard::load(string imgname)
{
	img_billboard = Assets::GetImage(imgname);
}

void NBillboard::render(float x, float y, float z)
{
	sf::Texture::bind(img_billboard);

	glBegin(GL_QUADS);
		glTexCoord2f(0.0F,0.0F); glVertex3f(x-0.5F,y+0.5F,z+0.0F);
		glTexCoord2f(1.0F,0.0F); glVertex3f(x+0.5F,y+0.5F,z+0.0F);
		glTexCoord2f(1.0F,1.0F); glVertex3f(x+0.5F,y-0.5F,z+0.0F);
		glTexCoord2f(0.0F,1.0F); glVertex3f(x-0.5F,y-0.5F,z+0.0F);
	glEnd();
}

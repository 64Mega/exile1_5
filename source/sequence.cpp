// Sequence implementation

#include <sequence.h>
#include <iostream>

using namespace std;

Sequence::Sequence(std::string filename)
{
    loadIntoTextures(filename);
}
Sequence::~Sequence()
{
    // Delete all textures
    for(vector<sf::Texture*>::iterator i = textures.begin(); i != textures.end(); i++)
    {
        sf::Texture* dptr = (*i);
        textures.erase(i);
        delete dptr;
        //if(i == textures.end()) break;
    }
}

int Sequence::animate() // Returns current frame
{
    if(ltype == LOOP_NONE)
    {
        if((framespeed > 0 && isEnd()) || (framespeed < 0 && isBeginning())) return c_frame;
        f_frame += framespeed;
        c_frame = (int)f_frame;
        if(c_frame > num_frames-1)
        {
            c_frame = num_frames-1;
        }
        if(c_frame < 0)
        {
            c_frame = 0;
        }
    }
    else
    if(ltype == LOOP_FORWARD)
    {
        f_frame += framespeed;
        c_frame = (int)f_frame;
        if(c_frame > num_frames-1)
        {
            c_frame = 0;
            f_frame = 0.0F;
        }
        if(c_frame < 0)
        {
            c_frame = num_frames-1;
            f_frame = (float)c_frame;
        }
    }
    else
    if(ltype == LOOP_PINGPONG)
    {
        f_frame += framespeed;
        c_frame = (int)f_frame;
        if(c_frame >= num_frames-1)
        {
            c_frame = num_frames-1;
            f_frame = (float)c_frame;
            framespeed = -framespeed;
        }
        else
        if(c_frame < 0)
        {
            c_frame = 0;
            f_frame = 0.0F;
            framespeed = -framespeed;
        }
    }

    return c_frame;
}

int Sequence::getCurrentFrame() // As on the tin
{
    return c_frame;
}

int Sequence::getNumFrames()
{
    return num_frames;
}

bool Sequence::isEnd()
{
    return c_frame == num_frames-1;
}

bool Sequence::isBeginning()
{
    return c_frame == 0;
}

void Sequence::setLoopType(looptype type)
{
    ltype = type;
}

void Sequence::setSpeed(float speed)
{
    framespeed = speed;
}

void Sequence::setFrame(int frame)
{
    c_frame = frame%num_frames;
    f_frame = (float)c_frame;
}

sf::Texture* Sequence::getTexture() // Returns the texture for the current frame
{
    return textures[c_frame];
}

// Reads the provided .seq file and loads all textures.

void Sequence::loadIntoTextures(std::string seqFileName)
{
    // A .seq file is a simple ASCII file containing one texture path per
    // line. An example would be:
    //   ./textures/1.png
    //   ./textures/2.png
    //   ./textures/3.png
    // .. etc ..

    ifstream fin(seqFileName.c_str());
    if(!fin.is_open())
    {
        throw "Unable to open Sequence '" + seqFileName + "'!";
    }

    vector<string> lines;
    string temp = "";
    while(!fin.eof())
    {
        temp = MIO::fgetline(fin);
        if(temp != "EOF") lines.push_back(temp);
    }

    for(int i = 0; i < lines.size(); i++)
    {
        sf::Texture *tex = new sf::Texture();
        tex->loadFromFile(lines[i]);
        tex->setSmooth(false);
        tex->setRepeated(false);

        textures.push_back(tex);
    }

    num_frames = textures.size();
    c_frame = 0;
    f_frame = 0.0F;

    ltype = LOOP_NONE;
    framespeed = 1.0F;

    fin.close();
}

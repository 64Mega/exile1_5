#include <gl/glew.h>
#include <model3ds.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

using namespace std;

Model3DS::Model3DS(std::string fname)
{
    // Load the model
    model = lib3ds_file_load(fname.c_str());

    f_begin = 0;
    f_end = 0;
    c_frame = 0;
    f_frame = 0.0F;
    framespeed = 1.0F;

    if(!model)
    {
        throw std::string("Couldn't open " + fname + "!!");
    }
}

Model3DS::~Model3DS()
{
    glDeleteBuffers(1, vertexVBO);
    glDeleteBuffers(1, normalVBO);
    glDeleteBuffers(1, textureVBO);
}

void Model3DS::cycle()
{
    // animates a frame of the model and updates the mesh VBO.
    f_frame += framespeed;
    c_frame = (int)f_frame;

    if(c_frame >= model->frames)
    {
        // Just forward-loop it for now.
        c_frame = 0;
        f_frame = 0.0F;
    }

    lib3ds_file_eval(model, f_frame);
    getFaces();

    Lib3dsMesh* mesh;

    Lib3dsVector* vertices = new Lib3dsVector[total_faces*3];
    Lib3dsVector* normals = new Lib3dsVector[total_faces*3];

    unsigned int finished_faces = 0;

    for(mesh = model->meshes; mesh != NULL; mesh = mesh->next)
    {
        lib3ds_mesh_calculate_normals(mesh, &normals[finished_faces*3]);

        for(unsigned int cur_face = 0; cur_face < mesh->faces; cur_face++)
        {
            Lib3dsFace* face = &mesh->faceL[cur_face];
            for(unsigned int i = 0; i < 3; i++)
            {
                memcpy(&vertices[(finished_faces*3)+i], mesh->pointL[face->points[i]].pos, sizeof(Lib3dsVector));
            }
            finished_faces++;
        }
    }

    // Write new data to vertex VBO.
    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);

    delete[] vertices;
    delete[] normals;
}

void Model3DS::getFaces()
{
    if(model == NULL)
    {
        throw "Can't count model with no faces!";
    }

    total_faces = 0;
    Lib3dsMesh* mesh;

    for(mesh = model->meshes; mesh != NULL; mesh = mesh->next)
    {
        total_faces += mesh->faces;
    }
}

void Model3DS::createVBO()
{
    if(model == NULL)
    {
        throw "No model loaded!";
    }

    getFaces();

    // Allocate memory
    Lib3dsVector* vertices = new Lib3dsVector[total_faces*3];
    Lib3dsVector* normals = new Lib3dsVector[total_faces*3];
    Lib3dsTexel* texels = new Lib3dsTexel[total_faces*3];

    Lib3dsMesh* mesh;
    unsigned int finished_faces = 0;

    for(mesh = model->meshes; mesh != NULL; mesh = mesh->next)
    {
        lib3ds_mesh_calculate_normals(mesh, &normals[finished_faces*3]);

        for(unsigned int cur_face = 0; cur_face < mesh->faces; cur_face++)
        {
            Lib3dsFace* face = &mesh->faceL[cur_face];
            for(unsigned int i = 0; i < 3; i++)
            {
                memcpy(&vertices[(finished_faces*3)+i], mesh->pointL[face->points[i]].pos, sizeof(Lib3dsVector));
            }

            Lib3dsTexel* tex = &mesh->texelL[cur_face];
            finished_faces++;
        }
    }

    glGenBuffers(1, &vertexVBO);
    glBindBuffer(GL_ARRAY_BUFFER, vertexVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(Lib3dsVector)*3*total_faces, vertices, GL_STREAM_DRAW);

    glGenBuffersARB(1, &normalVBO);
    glBindBufferARB(GL_ARRAY_BUFFER, normalVBO);
    glBufferDataARB(GL_ARRAY_BUFFER, sizeof(Lib3dsVector) * 3 * total_faces, normals, GL_STREAM_DRAW);

    glGenBuffersARB(1, &textureVBO);
    glBindBufferARB(GL_ARRAY_BUFFER, textureVBO);
    glBufferDataARB(GL_ARRAY_BUFFER, sizeof(Lib3dsTexel) * 3 * total_faces, normals, GL_STATIC_DRAW);

    delete[] vertices;
    delete[] normals;

    lib3ds_file_free(model);
    model = NULL;
}

void Model3DS::draw() const
{
    if(total_faces == 0)
    {
        throw "Empty mesh!";
    }

    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);

    glBindBufferARB(GL_ARRAY_BUFFER, normalVBO);
    glNormalPointer(GL_FLOAT, 0, NULL);

    glBindBufferARB(GL_ARRAY_BUFFER, vertexVBO);
    glVertexPointer(3, GL_FLOAT, 0, NULL);

    glBindBufferARB(GL_ARRAY_BUFFER, textureVBO);
    glVertexPointer(2, GL_FLOAT, 0, NULL);

    glDrawArrays(GL_TRIANGLES, 0, total_faces*3);

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_NORMAL_ARRAY);
}

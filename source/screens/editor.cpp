// Main boilerplate for the editor
// --

#include <screens/editor.h>
#include <editor/tileswatch.h>
#include <editor/eentswatch.h>
#include <editor/exitscreen.h>
#include <editor/edit3d.h>
#include <editor/save.h>
#include <editor/load.h>
#include <cstdio>
#include <iostream>

#include <math.h>

#include <spectrum/spectrum.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include <input.h>

#include <sstream>

using namespace std;

ScreenEditor::ScreenEditor()
{
    screenName = "ScreenEditor";
    cout << "[SCREEN] Entered Editor screen." << endl;

    tex_cursor.loadFromFile("./editor/cursor.png");
    done = false;

    view_x = 0;
    view_y = 0;

    c_tile = 0;
    c_eent = 0;
    mode = Mode_Select;
    wallmode = 0;

    camx = 0.0F;
    camz = 0.0F;
    camr = 45.0F;

    canExit = false;

    mapname = "";

    // Initialize map data
    d_floors = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_ceils = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_walls = new std::vector<T_EWall>(MAP_SIZE*MAP_SIZE);
    d_ents = new std::vector<T_EEntity>(MAP_SIZE*MAP_SIZE);
    d_lights = new std::vector<T_ELight>(MAP_SIZE*MAP_SIZE);

    mbLeft = false;
    mbRight = false;

    Input::init();
}

ScreenEditor::~ScreenEditor()
{
    cout << "[SCREEN] Removing Editor screen." << endl;

    if(d_floors) delete d_floors;
    if(d_ceils) delete d_ceils;
    if(d_walls) delete d_walls;
    if(d_ents) delete d_ents;
	if(d_lights) delete d_lights;
}

void ScreenEditor::Clear()
{
    delete d_floors;
    delete d_ceils;
    delete d_walls;
    delete d_ents;
    delete d_lights;

    d_floors = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_ceils = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_walls = new std::vector<T_EWall>(MAP_SIZE*MAP_SIZE);
    d_ents = new std::vector<T_EEntity>(MAP_SIZE*MAP_SIZE);
    d_lights = new std::vector<T_ELight>(MAP_SIZE*MAP_SIZE);
}

void drawLine(int x1, int y1, int x2, int y2)
{
    glBegin(GL_LINES);
    glVertex2i(x1,y1);
    glVertex2i(x2,y2);
    glEnd();
}

void drawCam(float x, float z, float r, int view_x, int view_y)
{
    // Calculate some lines
    float sx = (x*32)-view_x;
    float sy = (z*32)-view_y;
    float tx = cos(DEGTORADS(r))*32;
    float ty = sin(DEGTORADS(r))*32;

    float rr = r;
    rr -= 140;
    while(rr < 0)rr += 360.0F;

    float hx = cos(DEGTORADS(r+120))*8;
    float hy = sin(DEGTORADS(r+120))*8;

    float hhx = cos(DEGTORADS(rr))*8;
    float hhy = sin(DEGTORADS(rr))*8;


    sf::Texture::bind(NULL);
    glColor4f(0.2,0.7,1.0,1.0F);
    glBegin(GL_LINES);
        glVertex2f(sx,sy);
        glColor4f(1.0,0.7,0.2,1.0F);
        glVertex2f(sx+tx,sy+ty);
    glEnd();
}

void drawGrid(int vox, int voy)
{
    // Draws a 32x32 grid
    int vx = vox%32;
    int vy = voy%32;
    glColor3f(0.5,0.5,0.5);
    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            drawLine(vx+ix*32,0,vx+ix*32,600);
        }
        drawLine(0,vy+iy*32,800,vy+iy*32);
    }
}

void drawLevelFloor(int view_x, int view_y, std::vector<T_ETile>& tiles, ScreenEditor* map)
{
    // Draw a subsection of the level
    int sx = view_x/32;
    int sy = view_y/32;

    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            int tx = (sx+ix);
            int ty = (sy+iy);
            int index = ty*MAP_SIZE+tx;
            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Tile Lookup out of bounds!!" << endl;
            }
            else
            {
                if(tiles[index].tileindex < 0) continue;
                int cx = ((ix*32)-32)-(view_x%32);
                int cy = ((iy*32)-32)-(view_y%32);
                glColor4f(1,1,1,0.6F);
                sf::Texture::bind(Assets::GetTileByIndex(map->i_tiles[tiles[index].tileindex]));
                glBegin(GL_QUADS);
                glTexCoord2f(0,0);
                glVertex2i(cx,cy);
                glTexCoord2f(1,0);
                glVertex2i(cx+32,cy);
                glTexCoord2f(1,1);
                glVertex2i(cx+32,cy+32);
                glTexCoord2f(0,1);
                glVertex2i(cx,cy+32);
                glEnd();
                glColor4f(1,1,1,1);
            }
        }
    }
}

void drawLevelEnts(int view_x, int view_y, std::vector<T_EEntity>& tiles)
{
//	cout << "IN DRAWLEVELENTS..." << endl;
    // Draw a subsection of the level
    int sx = view_x/32;
    int sy = view_y/32;

    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            int tx = (sx+ix);
            int ty = (sy+iy);
            int index = ty*MAP_SIZE+tx;
            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Entity Lookup out of bounds!!" << endl;
            }
            else
            {
                if(tiles[index].type < 0) continue;
                int cx = ((ix*32)-32)-(view_x%32);
                int cy = ((iy*32)-32)-(view_y%32);
                glColor4f(1,1,1,0.8F);
                sf::Texture::bind(Assets::GetEEntByIndex(tiles[index].type));
                glBegin(GL_QUADS);
                glTexCoord2f(0,0);
                glVertex2i(cx,cy);
                glTexCoord2f(1,0);
                glVertex2i(cx+32,cy);
                glTexCoord2f(1,1);
                glVertex2i(cx+32,cy+32);
                glTexCoord2f(0,1);
                glVertex2i(cx,cy+32);
                glEnd();
                glColor4f(1,1,1,1);
            }
        }
    }

//	cout << "OUT DRAWLEVELENTS...." << endl;
}

void drawLevelCeiling(int view_x, int view_y, std::vector<T_ETile>& tiles, ScreenEditor* map)
{
//	cout << "IN DRAWLEVELCEILING..." << endl;
    // Draw a subsection of the level
    int sx = view_x/32;
    int sy = view_y/32;

    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            int tx = (sx+ix);
            int ty = (sy+iy);
            int index = ty*MAP_SIZE+tx;
            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Tile Lookup out of bounds!!" << endl;
            }
            else
            {
                if(tiles[index].tileindex < 0) continue;
                int cx = ((ix*32)-32)-(view_x%32);
                int cy = ((iy*32)-32)-(view_y%32);
                glColor4f(1,0.3,0.3,0.5);
                sf::Texture::bind(Assets::GetTileByIndex(map->i_tiles[tiles[index].tileindex]));
                glBegin(GL_QUADS);
                glTexCoord2f(0,0);
                glVertex2i(cx+8,cy+8);
                glTexCoord2f(1,0);
                glVertex2i(cx+24,cy+8);
                glTexCoord2f(1,1);
                glVertex2i(cx+24,cy+24);
                glTexCoord2f(0,1);
                glVertex2i(cx+8,cy+24);
                glEnd();
                glColor4f(1,1,1,1);
            }
        }
    }

//	cout << "OUT DRAWLEVELCEILING..." << endl;
}

void drawLevelLights(int view_x, int view_y, std::vector<T_ELight>& lights)
{
    int sx = view_x/32;
    int sy = view_y/32;

    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            int tx = (sx+ix);
            int ty = (sy+iy);
            int index = ty*MAP_SIZE+tx;

            if(index < 0 || index > lights.size()-1)
            {
                cout << "Light Lookup out of bounds!!" << endl;
            }
            else
            {
                if(lights[index].level > 0)
                {
                    glColor4f(1,1,1,0.95F);
                    sf::Texture::bind(NULL);
                    int cx = ((ix*32)-32)-(view_x%32);
                    int cy = ((iy*32)-32)-(view_y%32);
                    glBegin(GL_QUADS);
                        glVertex2i(cx+4,cy+4);
                        glVertex2i(cx+12,cy+4);
                        glVertex2i(cx+12,cy+12);
                        glVertex2i(cx+4,cy+12);
                    glEnd();
                }
            }
        }
    }
}

void drawLevelWalls(int view_x, int view_y, std::vector<T_EWall>& tiles, ScreenEditor* map)
{
//	cout << "IN DRAWLEVELWALLS" << endl;

    // Draw a subsection of the level
    int sx = view_x/32;
    int sy = view_y/32;

    for(int iy = 0; iy < 20; iy++)
    {
        for(int ix = 0; ix < 26; ix++)
        {
            int tx = (sx+ix);
            int ty = (sy+iy);
            int index = ty*MAP_SIZE+tx;
            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Wall Lookup out of bounds!!" << endl;
            }
            else
            {
                if(tiles[index].tileindex < 0) continue;
                int cx = ((ix*32)-32)-(view_x%32);
                int cy = ((iy*32)-32)-(view_y%32);
                glColor4f(1,0.9,0.9,1);
                sf::Texture::bind(Assets::GetTileByIndex(map->i_tiles[tiles[index].tileindex]));
                glBegin(GL_QUADS);

                if(tiles[index].type == 0) // Left wall
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx,cy);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+4,cy);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+4,cy+32);
                    glTexCoord2f(0,1);
                    glVertex2i(cx,cy+32);
                }
                if(tiles[index].type == 1) // Right wall
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx+32,cy);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+28,cy);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+28,cy+32);
                    glTexCoord2f(0,1);
                    glVertex2i(cx+32,cy+32);
                }
                if(tiles[index].type == 2) // Top wall
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx,cy);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+32,cy);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+32,cy+4);
                    glTexCoord2f(0,1);
                    glVertex2i(cx,cy+4);
                }
                if(tiles[index].type == 3) // Bottom wall
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx,cy+32);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+32,cy+32);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+32,cy+28);
                    glTexCoord2f(0,1);
                    glVertex2i(cx,cy+28);
                }
                if(tiles[index].type == 4) // Diagonal Left
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx,cy);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+4,cy);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+32,cy+32);
                    glTexCoord2f(0,1);
                    glVertex2i(cx+28,cy+32);
                }
                if(tiles[index].type == 5) // Diagonal Right
                {
                    glTexCoord2f(0,0);
                    glVertex2i(cx+28,cy);
                    glTexCoord2f(1,0);
                    glVertex2i(cx+32,cy);
                    glTexCoord2f(1,1);
                    glVertex2i(cx+4,cy+32);
                    glTexCoord2f(0,1);
                    glVertex2i(cx,cy+32);
                }
                glEnd();
                glColor4f(1,1,1,1);
            }
        }
    }

//	cout << "OUT DRAWLEVEL WALLS" << endl;
}

void drawStatusBar(int view_x, int view_y, int c_tile)
{
    // Draws a status bar overlay
    stringstream status;
    status << "VIEW POSITION: " << view_x << " : " << view_y;

    Engine::GetEngine().Print(0,0,status.str());

    // Draw current tile in bottom-right corner of screen,
    // with 75% alpha
    glColor4f(1,1,1,0.75F);
    sf::Texture::bind(Assets::GetTileByIndex(c_tile));
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);
    glVertex2i(766,566);
    glTexCoord2f(1,0);
    glVertex2i(798,566);
    glTexCoord2f(1,1);
    glVertex2i(798,598);
    glTexCoord2f(0,1);
    glVertex2i(766,598);
    glEnd();
    glColor4f(1,1,1,1);
}

void ScreenEditor::drawTile(int mx, int my)
{
    mx += 8;
    my += 8;

    std::vector<T_ETile>& tiles = mode == Mode_Floor ? (*d_floors) : (*d_ceils);

    int cx = (floor((mx+8)/32)*32)-(view_x%32);
    int cy = (floor((my+8)/32)*32)-(view_y%32);

    // Placing tiles
    if(Input::clickd(sf::Mouse::Left))
    {
        if(!mbLeft)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;//(1+cx/32)+(view_x/32); //(view_x/32)+(mx/32);
            int vvy = ((my+8)/32)+(view_y/32)+1;//(1+cy/32)+(view_y/32); //(view_y/32)+(my/32);
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Tiles out of bounds while placing!" << endl;
            }
            else
            {
				string tname = Assets::GetTileName(c_tile);
				// Search through current index listing
				bool found = false;

				for(int i = 0; i < t_tiles.size(); i++)
				{
					if(t_tiles[i] == tname) {
						found = true;
						tiles[index].tileindex = i;
						break;
					}	
				}

				if(!found)
				{
					t_tiles.push_back(tname);
					printf("DA HECK? %d\n",c_tile);
					i_tiles.push_back((int)(c_tile));
					tiles[index].tileindex = t_tiles.size()-1;
				}
            }
        }
    }
    else mbLeft = false;

    if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        if(!mbRight)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;//(1+cx/32)+(view_x/32); //(view_x/32)+(mx/32);
            int vvy = ((my+8)/32)+(view_y/32)+1;//(1+cy/32)+(view_y/32); //(view_y/32)+(my/32);
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Tiles out of bounds while placing!" << endl;
            }
            else
            {
                tiles[index].tileindex = -1;
            }
        }
    }
    else mbRight = false;

    // Draw 'shadow' tile underneath the mouse position, snapped to grid.
    glColor4f(1,1,1,0.75F);
    sf::Texture::bind(Assets::GetTileByIndex(c_tile));
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);
    glVertex2i(cx,cy);
    glTexCoord2f(1,0);
    glVertex2i(cx+32,cy);
    glTexCoord2f(1,1);
    glVertex2i(cx+32,cy+32);
    glTexCoord2f(0,1);
    glVertex2i(cx,cy+32);
    glEnd();
    glColor4f(1,1,1,1);
    sf::Texture::bind(0);

    // Draw outline of tile.
    glColor3f(1,0,1);
    glBegin(GL_LINE_STRIP);
    glVertex2i(cx,cy);
    glVertex2i(cx+32,cy);
    glVertex2i(cx+32,cy+32);
    glVertex2i(cx,cy+32);
    glVertex2i(cx,cy);
    glEnd();
}

void ScreenEditor::drawEnts(int mx, int my)
{
    mx += 8;
    my += 8;

    std::vector<T_EEntity>& tiles = (*d_ents);

    int cx = (floor((mx+8)/32)*32)-(view_x%32);
    int cy = (floor((my+8)/32)*32)-(view_y%32);

    // Placing entities
    if(Input::clickd(sf::Mouse::Left))
    {
        if(!mbLeft)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Entity out of bounds while placing!" << endl;
            }
            else
            {
                tiles[index].type = c_eent;
            }
        }
    }
    else mbLeft = false;

    if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        if(!mbRight)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Entity out of bounds while removing!" << endl;
            }
            else
            {
                tiles[index].type = -1;
            }
        }
    }
    else mbRight = false;

    // Draw 'shadow' tile underneath the mouse position, snapped to grid.
    glColor4f(1,1,1,0.75F);
    sf::Texture::bind(Assets::GetEEntByIndex(c_eent));
    glBegin(GL_QUADS);
    glTexCoord2f(0,0);
    glVertex2i(cx,cy);
    glTexCoord2f(1,0);
    glVertex2i(cx+32,cy);
    glTexCoord2f(1,1);
    glVertex2i(cx+32,cy+32);
    glTexCoord2f(0,1);
    glVertex2i(cx,cy+32);
    glEnd();
    glColor4f(1,1,1,1);
    sf::Texture::bind(0);

    // Draw outline of tile.
    glColor3f(1,0,1);
    glBegin(GL_LINE_STRIP);
    glVertex2i(cx,cy);
    glVertex2i(cx+32,cy);
    glVertex2i(cx+32,cy+32);
    glVertex2i(cx,cy+32);
    glVertex2i(cx,cy);
    glEnd();
}

void ScreenEditor::drawLights(int mx, int my)
{
    mx += 8;
    my += 8;

    std::vector<T_ELight>& lights = (*d_lights);

    int cx = (floor((mx+8)/32)*32)-(view_x%32);
    int cy = (floor((my+8)/32)*32)-(view_y%32);

    // Placing entities
    if(Input::clickd(sf::Mouse::Left))
    {
        if(!mbLeft)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > lights.size()-1)
            {
                cout << "Light out of bounds while placing!" << endl;
            }
            else
            {
                lights[index].level = 1;
            }
        }
    }
    else mbLeft = false;

    if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        if(!mbRight)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > lights.size()-1)
            {
                cout << "Light out of bounds while removing!" << endl;
            }
            else
            {
                lights[index].level = 0;
            }
        }
    }
    else mbRight = false;

    // Draw 'shadow' tile underneath the mouse position, snapped to grid.
    glColor4f(1,1,1,0.75F);
    sf::Texture::bind(0);
    glBegin(GL_QUADS);
    glVertex2i(cx,cy);
    glVertex2i(cx+32,cy);
    glVertex2i(cx+32,cy+32);
    glVertex2i(cx,cy+32);
    glEnd();
    glColor4f(1,1,1,1);
    sf::Texture::bind(0);

    // Draw outline of tile.
    glColor3f(1,0,1);
    glBegin(GL_LINE_STRIP);
    glVertex2i(cx,cy);
    glVertex2i(cx+32,cy);
    glVertex2i(cx+32,cy+32);
    glVertex2i(cx,cy+32);
    glVertex2i(cx,cy);
    glEnd();
}

void ScreenEditor::drawWall(int mx, int my)
{
    mx += 8;
    my += 8;

    int cx = (floor((mx+8)/32)*32)-(view_x%32);
    int cy = (floor((my+8)/32)*32)-(view_y%32);

    std::vector<T_EWall>& tiles = *d_walls;
    int type = wallmode;

    // Placing tiles
    if(Input::clickd(sf::Mouse::Left))
    {
        if(!mbLeft)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Wall out of bounds while placing!" << endl;
            }
            else
            {

				bool found = false;
				string tname = Assets::GetTileName(c_tile);
				
				for(int i = 0; i < t_tiles.size(); i++)
				{
					if(t_tiles[i] == tname) {
						found = true;
						tiles[index].tileindex = i;
						break;
					}	
				}

				if(!found) {
					t_tiles.push_back(tname);
					i_tiles.push_back(c_tile);
					tiles[index].tileindex = t_tiles.size()-1;
				}
                
                tiles[index].type = type;
            }
        }
    }
    else mbLeft = false;

    if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
    {
        if(!mbRight)
        {
            int vvx = ((mx+8)/32)+(view_x/32)+1;
            int vvy = ((my+8)/32)+(view_y/32)+1;
            int index = vvy*MAP_SIZE+vvx;

            if(index < 0 || index > tiles.size()-1)
            {
                cout << "Wall out of bounds while deleting!" << endl;
            }
            else
            {
                tiles[index].tileindex = -1;
            }
        }
    }
    else mbRight = false;

    glColor4f(0.8,1,1,1.0F);
    sf::Texture::bind(Assets::GetTileByIndex(c_tile));
    glBegin(GL_QUADS);
    if(type == 0) // Left wall
    {
        glTexCoord2f(0,0);
        glVertex2i(cx,cy);
        glTexCoord2f(1,0);
        glVertex2i(cx+4,cy);
        glTexCoord2f(1,1);
        glVertex2i(cx+4,cy+32);
        glTexCoord2f(0,1);
        glVertex2i(cx,cy+32);
    }
    if(type == 1) // Right wall
    {
        glTexCoord2f(0,0);
        glVertex2i(cx+32,cy);
        glTexCoord2f(1,0);
        glVertex2i(cx+28,cy);
        glTexCoord2f(1,1);
        glVertex2i(cx+28,cy+32);
        glTexCoord2f(0,1);
        glVertex2i(cx+32,cy+32);
    }
    if(type == 2) // Top wall
    {
        glTexCoord2f(0,0);
        glVertex2i(cx,cy);
        glTexCoord2f(1,0);
        glVertex2i(cx+32,cy);
        glTexCoord2f(1,1);
        glVertex2i(cx+32,cy+4);
        glTexCoord2f(0,1);
        glVertex2i(cx,cy+4);
    }
    if(type == 3) // Bottom wall
    {
        glTexCoord2f(0,0);
        glVertex2i(cx,cy+32);
        glTexCoord2f(1,0);
        glVertex2i(cx+32,cy+32);
        glTexCoord2f(1,1);
        glVertex2i(cx+32,cy+28);
        glTexCoord2f(0,1);
        glVertex2i(cx,cy+28);
    }
    if(type == 4) // Diagonal Left
    {
        glTexCoord2f(0,0);
        glVertex2i(cx,cy);
        glTexCoord2f(1,0);
        glVertex2i(cx+4,cy);
        glTexCoord2f(1,1);
        glVertex2i(cx+32,cy+32);
        glTexCoord2f(0,1);
        glVertex2i(cx+28,cy+32);
    }
    if(type == 5) // Diagonal Right
    {
        glTexCoord2f(0,0);
        glVertex2i(cx+28,cy);
        glTexCoord2f(1,0);
        glVertex2i(cx+32,cy);
        glTexCoord2f(1,1);
        glVertex2i(cx+4,cy+32);
        glTexCoord2f(0,1);
        glVertex2i(cx,cy+32);
    }
    glEnd();
    sf::Texture::bind(0);
}

void ScreenEditor::update()
{
    // From here, treat it as a completely standalone program to make things simpler.

    Engine& engine = Engine::GetEngine();

    // Execute any running child screens
    if(screenlist.empty() != true)
    {
        screenlist.back()->update();
        if(screenlist.back()->isDone())
        {
            delete screenlist.back();
            screenlist.pop_back();
            return;
        }
        return;
    }

    if(canExit)
    {
        done = true;

        while(screenlist.empty() != true)
        {
            delete screenlist.back();
            screenlist.pop_back();
        }
    }

    // Temporary escape
    // Add a "Do you want to save?" screen.
    if(Input::keyPressed(sf::Keyboard::Escape))
    {
        screenlist.push_back(new ExitScreen(this));
        return;
    }

    // View panning
    if(Input::keyDown(sf::Keyboard::A)) view_x -= 2;
    if(Input::keyDown(sf::Keyboard::D)) view_x += 2;
    if(Input::keyDown(sf::Keyboard::W)) view_y -= 2;
    if(Input::keyDown(sf::Keyboard::S)) view_y += 2;

    // Clamp view to map bounds
    if(view_x < 0)view_x = 0;
    if(view_y < 0)view_y = 0;
    if(view_x > (MAP_SIZE*32)-800) view_x = (MAP_SIZE*32)-800;
    if(view_y > (MAP_SIZE*32)-600) view_y = (MAP_SIZE*32)-600;

    // Bring up the Tile Swatch
    if(Input::keyPressed(sf::Keyboard::F1))
    {
        screenlist.push_back(new TileSwatch(this));
        mode = Mode_Select;
        mbLeft = true;
    }
    // Bring up the EEnt Swatch
    if(Input::keyPressed(sf::Keyboard::F2))
    {
        screenlist.push_back(new EEntSwatch(this));
        mode = Mode_Select;
        mbLeft = true;
    }
    // Go to the 3D editor
    if(Input::keyPressed(sf::Keyboard::F3))
    {
        screenlist.push_back(new Edit3D(this));
        mode = Mode_Select;
        mbLeft = true;
    }
    // Bring up the Save screen
    if(Input::keyPressed(sf::Keyboard::F5))
    {
        screenlist.push_back(new SaveScreen(this));
        mode = Mode_Select;
    }
    // Bring up the Load screen
    if(Input::keyPressed(sf::Keyboard::F6))
    {
        screenlist.push_back(new LoadScreen(this));
        mode = Mode_Select;
    }


    // Mode switching
    if(Input::keyPressed(sf::Keyboard::Num1)) mode = Mode_Select;
    if(Input::keyPressed(sf::Keyboard::Num2)) mode = Mode_Floor;
    if(Input::keyPressed(sf::Keyboard::Num3)) mode = Mode_Ceiling;
    if(Input::keyPressed(sf::Keyboard::Num4)) mode = Mode_Wall;
    if(Input::keyPressed(sf::Keyboard::Num5)) mode = Mode_Ents;
    if(Input::keyPressed(sf::Keyboard::Num6)) mode = Mode_Lights;

    // Get mouse coordinates
    int mx = sf::Mouse::getPosition(engine.GetRenderContext()).x;
    int my = sf::Mouse::getPosition(engine.GetRenderContext()).y;

    engine.go2D();
    glDisable(GL_DEPTH_TEST);
    drawGrid(-view_x, -view_y);
    drawLevelFloor(view_x,view_y,(*d_floors), this);
    drawLevelWalls(view_x,view_y,(*d_walls), this);
    drawCam(camx,camz,camr,view_x,view_y);
    drawLevelEnts(view_x,view_y,(*d_ents));

    // Do current mode processing

    if(mode == Mode_Select)
    {
        // On Right click, set cam position
        if(Input::click(sf::Mouse::Right))
        {
            camx = (mx+view_x)/32;
            camz = (my+view_y)/32;
        }
    }
    if(mode == Mode_Ceiling)
    {
        drawLevelCeiling(view_x,view_y,(*d_ceils), this);
    }

    if(mode == Mode_Floor || mode == Mode_Ceiling)
    {
        drawTile(mx,my);
    }
    if(mode == Mode_Ents)
    {
        drawEnts(mx,my);
    }
    if(mode == Mode_Wall)
    {
        drawWall(mx,my);

        if(Input::keyPressed(sf::Keyboard::Up)) wallmode = 2;
        if(Input::keyPressed(sf::Keyboard::Down)) wallmode = 3;
        if(Input::keyPressed(sf::Keyboard::Left)) wallmode = 0;
        if(Input::keyPressed(sf::Keyboard::Right)) wallmode = 1;
        if(Input::keyPressed(sf::Keyboard::Delete)) wallmode = 4;
        if(Input::keyPressed(sf::Keyboard::PageDown)) wallmode = 5;
    }
    drawStatusBar(view_x, view_y, c_tile);

    if(mode == Mode_Lights)
    {
        drawLevelLights(view_x,view_y,(*d_lights));
        drawLights(mx,my);
    }

    if(mode == Mode_Select)
    {
        // Temporary cursor drawing
        sf::Texture::bind(&tex_cursor);
        glColor3f(1,1,1);
        glBegin(GL_QUADS);
        glTexCoord2f(0,0);
        glVertex2i(mx,my);
        glTexCoord2f(1,0);
        glVertex2i(mx+16,my);
        glTexCoord2f(1,1);
        glVertex2i(mx+16,my+16);
        glTexCoord2f(0,1);
        glVertex2i(mx,my+16);
        glEnd();
    }

    engine.Render();
}

void ScreenEditor::save(std::string filename)
{
    this->mapname = filename;
    cout << "[EDITOR] Saving '" << filename+".MAP" << "'!" << endl;

    // Save to simple binary format. UPDATE: Includes simple table data now.
	//
    // MAP identifier
    // Map dimensions (128x128)
	// Number of Tile Table entries
	// Pairs of <int32><cstring with length of int32>, each one corresponding to a tile identifier.
    // FloorData
    // CeilData
    // WallData
    // -- Can add other data later
    // Each entry is 4 bytes, and each tile takes up 12 bytes.

    string mapname = "./maps/" + filename + ".MAP";
    cout << mapname << endl;
    FILE* fout = fopen(mapname.c_str(),"wb"); // Derp.
    if(!fout)
    {
        cout << "[EDITOR] Error opening '" + mapname + "' for saving!" << endl;
        throw 0;
    }

    string headerstr = "MAP";

    int dimensions = MAP_SIZE;

	// Write identifier
	fwrite(headerstr.c_str(), headerstr.length(), sizeof(char), fout);

	fwrite(&dimensions, 1, sizeof(int), fout);

	// Write table data
	int tt_size = t_tiles.size();
	fwrite(&tt_size, 1, sizeof(int), fout);
	for(int i = 0; i < t_tiles.size(); i++) 
	{
		int tt_len = t_tiles[i].length();
		fwrite(&tt_len, 1, sizeof(int), fout);
		fwrite(t_tiles[i].c_str(), t_tiles[i].length(), sizeof(char), fout);
	}

    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fwrite(&(*d_floors)[i].tileindex,1,sizeof(int),fout);
        fwrite(&(*d_floors)[i].height,1,sizeof(int),fout);
        fwrite(&(*d_floors)[i].meta,1,sizeof(int),fout);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fwrite(&(*d_walls)[i].tileindex,1,sizeof(int),fout);
        fwrite(&(*d_walls)[i].type,1,sizeof(int),fout);
        fwrite(&(*d_walls)[i].meta,1,sizeof(int),fout);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fwrite(&(*d_ceils)[i].tileindex,1,sizeof(int),fout);
        fwrite(&(*d_ceils)[i].height,1,sizeof(int),fout);
        fwrite(&(*d_ceils)[i].meta,1,sizeof(int),fout);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fwrite(&(*d_ents)[i].type,1,sizeof(int),fout);
        fwrite(&(*d_ents)[i].height,1,sizeof(int),fout);
        fwrite(&(*d_ents)[i].meta,1,sizeof(int),fout);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fwrite(&(*d_lights)[i].level,1,sizeof(int),fout);
    }

    fclose(fout);
}

void ScreenEditor::load(std::string filename)
{
    this->mapname = filename;
    string mapname = "./maps/" + filename + ".MAP";
    FILE* fin = fopen(mapname.c_str(),"rb"); // Derp.
    if(!fin)
    {
        cout << "[EDITOR] Error opening '" + mapname + "' for loading!" << endl;
        return;
    }

    Clear();

    int dimensions = MAP_SIZE;
	char *mapid = new char[4];
	fread(mapid, 3, sizeof(char), fin);
	mapid[3] = '\0';

	printf("MAP ID: %s\n", mapid);
	if(string(mapid) != "MAP\0") 
	{
		printf("Map Identifier not matching \"MAP\". Please check that the file is not corrupted.\n");
		return;
	}
	delete[] mapid;
	
	// Read dimensions. Currently useless.
	fread(&dimensions, 1, sizeof(int), fin);
	printf("Dimensions: %d\n", dimensions);

	// Read in tile table. Clear current one.
	while(t_tiles.size() > 0) t_tiles.pop_back();
	while(i_tiles.size() > 0) i_tiles.pop_back();

	int tableLen;
	fread(&tableLen, 1, sizeof(int), fin);
	printf("Number of entries in Tile Table: %d\n", tableLen);

	for(int i = 0; i < tableLen; i++)
	{
		int slen;
		char* sbuf;

		fread(&slen, 1, sizeof(int), fin);
		sbuf = new char[slen+1];
		fread(sbuf, slen, sizeof(char), fin);
		sbuf[slen] = '\0';

		// Do lookup now to save time.
		t_tiles.push_back(sbuf);
		i_tiles.push_back(Assets::GetTileNumber(sbuf));
		
		delete[] sbuf;
	}

	// Load tile chunks

    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_floors)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_floors)[i].height,1,sizeof(int),fin);
        fread(&(*d_floors)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_walls)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_walls)[i].type,1,sizeof(int),fin);
        fread(&(*d_walls)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_ceils)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_ceils)[i].height,1,sizeof(int),fin);
        fread(&(*d_ceils)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_ents)[i].type,1,sizeof(int),fin);
        fread(&(*d_ents)[i].height,1,sizeof(int),fin);
        fread(&(*d_ents)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_lights)[i].level,1,sizeof(int),fin);
    }

    for(int i = 0; i < (*d_lights).size(); i++)
    {
        if((*d_lights)[i].level > 0)
        {
            cout << "LIGHT IN LEVEL... " << (*d_lights)[i].level << endl;
        }
    }

    fclose(fin);
}

std::vector<T_ETile>& ScreenEditor::floors()
{
    return (*d_floors);
}

std::vector<T_ETile>& ScreenEditor::ceils()
{
    return (*d_ceils);
}

std::vector<T_EWall>& ScreenEditor::walls()
{
    return (*d_walls);
}

std::vector<T_EEntity>& ScreenEditor::ents()
{
    return (*d_ents);
}

std::vector<T_ELight>& ScreenEditor::lights()
{
    return (*d_lights);
}

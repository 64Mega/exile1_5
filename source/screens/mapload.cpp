// Implementation of mapload.h

#include <screens/mapload.h>
#include <sfml/graphics.hpp>
#include <gl/gl.h>
#include <gl/glu.h>
#include <cstdio>
#include <string>
#include <iostream>
#include <config.h>

#include <spectrum/spectrum.h>

using namespace std;

MapLoadScreen::MapLoadScreen() {
    parent = NULL;
    done = false;
    screenName = "MapLoadScreen";
    cout << "[SCREEN] Entered Map Load Screen." << endl;
}

MapLoadScreen::MapLoadScreen(ScreenMainMenu* p) {
    parent = p;
    done = false;
    screenName = "MapLoadScreen";
    cout << "[SCREEN] Entered Map Load Screen." << endl;
}

MapLoadScreen::~MapLoadScreen()
{
    parent = NULL;
    cout << "Exiting Map Load Screen." << endl;
}

void MapLoadScreen::update()
{
    Engine& engine = Engine::GetEngine();
    engine.go2DX();

    string buffer = "";
    bool finished = false;
    char c;
    sf::Event ev;
    while(!finished)
    {
    while(engine.GetRenderContext().pollEvent(ev))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        engine.Print(0,0,"LOAD MAP:");
        engine.Print(0,10,"> ");

        if(ev.type == sf::Event::KeyPressed)
        {
            if(ev.key.code <= 25)
            {
                if(ev.key.shift) buffer += ev.key.code + 65;
                else buffer += ev.key.code + 65;
            }
            if(ev.key.code > 25 && ev.key.code <= 35)
            {
                buffer += ev.key.code-26 + 48;
            }
            if(ev.key.code == sf::Keyboard::Period)
            {
                buffer += '.';
            }
            if(ev.key.code == sf::Keyboard::BackSpace)
            {
                buffer = buffer.substr(0,buffer.size()-1);
            }
            if(ev.key.code == sf::Keyboard::Return)
            {
                if(buffer.empty() != true)
                {
                    global_config.current_map = string(buffer);
                    done = true;
                    finished = true;
                }
            }
            engine.Print(16,10,buffer);
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            finished = true;
            done = true;
        }
        engine.Print(16,10,buffer);
        engine.Render();
    }
    }

    engine.Render();
}




// 3D Editor implementation

#include <editor/edit3d.h>
#include <vector>
#include <string>
#include <sstream>
#include <map.h>
#include <player.h>
#include <screens/editor.h>
#include <editor/tileswatch.h>

#include <gl/gl.h>
#include <gl/glu.h>

#include <mouselook.h>
#include <input.h>

Edit3D::Edit3D(ScreenEditor* p)
{
    done = false;
    screenName = "Edit3D";
    cout << "[SCREEN] Entered Edit3D screen" << endl;

    x = y = z = 0.0F;
    rx = 0.0F;
    ry = 0.0F;

    parent = p;

    if(parent)
    {
        x = parent->camx;
        z = parent->camz;
        ry = parent->camr;
    }

    pmap.clear();

    // Transfer editor map data to new map
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        // DERP.
        pmap.floors()[i] = parent->floors()[i];
        pmap.ceils()[i] = parent->ceils()[i];
        pmap.walls()[i] = parent->walls()[i];
        pmap.ents()[i] = parent->ents()[i];
    }

	// Transfer tile table data over to new map
	for(int i = 0; i < parent->t_tiles.size(); i++) 
	{
		pmap.t_tiles.push_back(parent->t_tiles[i]);
		pmap.i_tiles.push_back(parent->i_tiles[i]);
	}

    MouseLook::init();

    shaders = true;
}

Edit3D::~Edit3D()
{
    cout << "[SCREEN] Left Edit3D screen" << endl;
}

void Edit3D::update()
{
    Engine& engine = Engine::GetEngine();
    MouseLook::update();

    ry += MouseLook::deltax * MOUSE_STRENGTH;
    rx -= MouseLook::deltay * MOUSE_STRENGTH;

    float sryl = ry - 90.0F;

    while(ry < 0.0F) ry += 360.0F;
    while(ry > 359.0F) ry -= 360.0F;
    while(sryl < 0.0F) sryl += 360.0F;
    while(sryl > 359.0F) sryl -= 360.0F;

    if(rx < -89.0F) rx = -89.0F;
    if(rx > 89.0F) rx = 89.0F;

    if(Input::keyPressed(sf::Keyboard::Tab))
    {
        shaders = !shaders;
    }

    // More Derp.
    engine.go3D();
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    float vx = cos(DEGTORADS(ry));
    float vz = sin(DEGTORADS(ry));
    float vxr = cos(DEGTORADS(sryl));
    float vzr = sin(DEGTORADS(sryl));

    float camheight = 0.0F;
    int indexp = (int)(floor(z)*MAP_SIZE+floor(x));

    if(indexp >= 0 && indexp < MAP_SIZE*MAP_SIZE) // Derp
    {
        if(parent->floors()[indexp].tileindex >= 0)
        {
            if(y < parent->floors()[indexp].height/8.0F)y += 0.1F;
            if(y > parent->floors()[indexp].height/8.0F)y -= 0.1F;
        }
    }

    gluLookAt(x, y+1.0F, z, x+vx, y+1.0F+tan(DEGTORADS(rx)), z+vz,0,1,0); // Derp
    pplayer.pos.x = x;
    pplayer.pos.y = y;
    pplayer.pos.z = z;
    pplayer.rot.y = ry;
    pplayer.rot.x = rx;

    pmap.render((int)round(x),(int)round(z),true, shaders);

    // Exit code
    if(Input::keyPressed(sf::Keyboard::Escape))
    {
        // Set 2D editor's cam positions
        parent->camx = x;
        parent->camz = z;
        parent->camr = ry;
        done = true;
        return;
    }

    // Floor modification
    if(pmap.floorpiece >= 0)
    {
        // Modify height
        if(Engine::mousewheel != 0)
        {
            pmap.floors()[pmap.floorpiece].height += Engine::mousewheel;
            parent->floors()[pmap.floorpiece].height += Engine::mousewheel;
        }

        // Paste texture to metadata
        if(Input::click(sf::Mouse::Left))
        {
			int ttile = -1;
			string ss = Assets::GetTileName(parent->c_tile);
			for(int i = 0; i < parent->t_tiles.size(); i++) 
			{
				if(parent->t_tiles[i] == ss) 
				{
					ttile = i;
					break;
				}
			}

			if(ttile >= 0) 
			{
	            pmap.floors()[pmap.floorpiece].meta = ttile;
	            parent->floors()[pmap.floorpiece].meta = ttile;
			}
			else
			{
				parent->t_tiles.push_back(ss);
				parent->i_tiles.push_back(parent->c_tile);
				pmap.floors()[pmap.floorpiece].meta = parent->t_tiles.size()-1;
	            parent->floors()[pmap.floorpiece].meta = parent->t_tiles.size()-1;
			}
        }
        if(Input::click(sf::Mouse::Right))
        {
            parent->screenlist.push_back(new TileSwatch(parent));
            return;
        }
    }
    else if(pmap.ceilpiece >= 0)
    {
        if(Engine::mousewheel != 0)
        {
            pmap.ceils()[pmap.ceilpiece].height += Engine::mousewheel;
            parent->ceils()[pmap.ceilpiece].height += Engine::mousewheel;
        }
        // Paste texture to metadata
        if(Input::click(sf::Mouse::Left))
        {
			int ttile = -1;
			string ss = Assets::GetTileName(parent->c_tile);
			for(int i = 0; i < parent->t_tiles.size(); i++) 
			{
				if(parent->t_tiles[i] == ss) 
				{
					ttile = i;
					break;
				}
			}

			if(ttile >= 0) 
			{
	            pmap.ceils()[pmap.ceilpiece].meta = ttile;
	            parent->ceils()[pmap.ceilpiece].meta = ttile;
			}
			else
			{
				parent->t_tiles.push_back(ss);
				parent->i_tiles.push_back(parent->c_tile);
				pmap.ceils()[pmap.ceilpiece].meta = parent->t_tiles.size()-1;
	            parent->ceils()[pmap.ceilpiece].meta = parent->t_tiles.size()-1;
			}

            // pmap.ceils()[pmap.ceilpiece].meta = parent->c_tile;
            // parent->ceils()[pmap.ceilpiece].meta = parent->c_tile;
        }
        if(Input::click(sf::Mouse::Right))
        {
            parent->screenlist.push_back(new TileSwatch(parent));
            return;
        }
    }
    else if(pmap.wallpiece >= 0)
    {
        // Paste texture to tileindex
        if(Input::click(sf::Mouse::Left))
        {
            //pmap.walls()[pmap.wallpiece].tileindex = parent->c_tile;
            //parent->walls()[pmap.wallpiece].tileindex = parent->c_tile;
        }
        if(Input::click(sf::Mouse::Right))
        {
            parent->screenlist.push_back(new TileSwatch(parent));
            return;
        }
    }



    // Movement
    if(Input::keyDown(sf::Keyboard::W))
    {
        x += vx*0.1;
        z += vz*0.1;
    }
    if(Input::keyDown(sf::Keyboard::S))
    {
        x -= vx*0.1;
        z -= vz*0.1;
    }
    if(Input::keyDown(sf::Keyboard::A))
    {
        x += vxr*0.1;
        z += vzr*0.1;
    }
    if(Input::keyDown(sf::Keyboard::D))
    {
        x -= vxr*0.1;
        z -= vzr*0.1;
    }

    // Temporary look code
    if(Input::keyDown(sf::Keyboard::Left))
    {
        ry -= 1.0F;
        while(ry < 0.0F) ry += 360.0F;
    }
    if(Input::keyDown(sf::Keyboard::Right))
    {
        ry += 1.0F;
        while(ry > 359.0F) ry -= 360.0F;
    }

    stringstream coords;
    coords << "X: " << x << " - Z: " << z;
    engine.go2D();
    engine.Print(0,0,coords.str());
    engine.Render();
}

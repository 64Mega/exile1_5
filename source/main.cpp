// Exile 1.5
// Built on the Spectrum Engine, version 1.6.7
// --
// (c) 2013,2014 Rewired Games
// --

#include <gl/glew.h>

#include <gl/gl.h>
#include <gl/glu.h>
#include <mgl_util.h>
#include "lib3ds.h"

#include <mio.h>
#include <math.h>
#include <mxmath.h>
#include <iostream>
#include <fstream>

std::ofstream lout; // Log

#include "EmuStream.h"

#include "spectrum/spectrum.h"
#include "config.h"

// Include various screens
#include "screens/options.h"
#include "screens/mainmenu.h"
#include "screens/gamemenu.h"

#include <map.h>
#include <model3ds.h>
#include <sequence.h>
#include <player.h>
#include <entity.h>
#include <input.h>
#include <entman.h>

#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glext.h>

#include <lua/lua.h>
#include <slb.hpp>

bool stateMainMenu;
bool stateGameMenu;

#define SQR(a) (a*a)

extern Sprite* font_game;

// Add entities to EntMan proto list
void addProtos()
{
    EntMan::getCurrent()->addProto(new Player());
}

int main(int argc, char** argv)
{
    GameLog::Initialize();

    stateMainMenu = true;
    stateGameMenu = false;
    bool shaders = true;

    bool kbEscape = false;

    global_config.Load();

    // Quick Entity test

    // -- End Entity test

    Assets::SetMusicVolume(global_config.music_volume);
    Assets::SetSoundVolume(global_config.sound_volume);
    if(global_config.music == false)Assets::SetMusicVolume(0);
    if(global_config.sound == false)Assets::SetSoundVolume(0);

    Engine* engine;
    engine = new Engine(global_config.width,global_config.height,global_config.fullscreen);
	engine->GetRenderContext().clear(sf::Color::Black);
	glClearColor(0.0,0.0,0.0,1.0);
	glClearDepth(1.0);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glColor3f(1.0f,1.0f,1.0f);

	// Display loading screen
	sf::Texture tex_loadscreen;
	tex_loadscreen.loadFromFile("temp_loadscreen.png");
	tex_loadscreen.setSmooth(false);
	
	sf::Texture::bind(&tex_loadscreen);

    glBegin(GL_QUADS);
		glTexCoord2f(0.0f,0.0f); glVertex2i(0  ,0);
		glTexCoord2f(1.0f,0.0f); glVertex2i(512,0);
		glTexCoord2f(1.0f,1.0f); glVertex2i(512,512);
		glTexCoord2f(0.0f,1.0f); glVertex2i(0  ,512);
	glEnd();

	glFlush();
	engine->GetRenderContext().display();
    // Initialize GLEW

    GLenum err = glewInit();
    if(GLEW_OK != err)
    {
        throw "Error initializing GLEW.";
    }

    sf::Image ico_darkhold;
    ico_darkhold.loadFromFile("./icon.png");
    engine->GetRenderContext().setIcon(128,128,ico_darkhold.getPixelsPtr());

    engine->GetRenderContext().setMouseCursorVisible(false);
    engine->GetRenderContext().setFramerateLimit(60);

	glClearColor(0.0,0.0,0.0,1.0);
    
	engine->ff_phase = 1;
    engine->ff_alpha = 0.0F;
	
    Assets::LoadPack(global_config.pack_assets);
    Assets::LoadList(global_config.list_assets);

    font_game = new Sprite("font_game",96);

    bool gamestarted = false;
    bool maploaded = false;

    Assets::svolume = global_config.sound_volume;
    Assets::mvolume = global_config.music_volume;
    Assets::music = global_config.music;
    Assets::sound = global_config.sound;

    cout <<"[SYSTEM] Game started." << endl;

    screen_list.push_back(new ScreenMainMenu());

    Map map;
    float rx = 0.0F;

    EntMan entmanager;
    addProtos();
    entmanager.mapSpawn();

    float scalx = 0.2F;

    while(engine->IsRunning())
    {
        glClearColor(0.0F,0.0F,0.0F,1.0F);
        engine->Update();

        rx+=0.2F;
        if(!engine->IsActive())
        {
            engine->Render();
            sf::sleep(sf::milliseconds(200));
            continue;
        }
        glDisable(GL_DEPTH_TEST);
        glCullFace(GL_BACK);
        glFrontFace(GL_CW);

        Assets::svolume = global_config.sound_volume;
        Assets::mvolume = global_config.music_volume;
        Assets::music = global_config.music;
        Assets::sound = global_config.sound;

        if(screen_list.empty() != true)
        {
            if(screen_list.back()->isDone())
            {
                delete screen_list.back();
                screen_list.pop_back();
            }
            else
            {
                screen_list.back()->update();
            }
            continue;
        }

        // Check if Escape is pressed

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) || sf::Joystick::isButtonPressed(0,7))
        {
            if(!kbEscape)
            {
                screen_list.push_back(new ScreenGameMenu());
            }

            kbEscape = true;
        }
        else kbEscape = false;

        engine->go3D();

        // Render stuff in 3D here
        glEnable(GL_DEPTH_TEST);

        // Load map if it isn't loaded
        if(global_config.current_map != map.name) maploaded = false;

        if(!maploaded)
        {
            map.load(global_config.current_map);

            maploaded = true;
            //player.pos = map.getPlayerStart();

            entmanager.mapSpawn();
            cout << "Loaded map." << endl;
        }
        else
        {
            //player.update();
            entmanager.update();

            if(Input::keyPressed(sf::Keyboard::Tab))
            {
                shaders = !shaders;
            }

            int vvx = (int)round(Player::getPlayer()->pos.x); // DERP
            int vvy = (int)round(Player::getPlayer()->pos.z);

            glColor4f(1,1,1,1);

            map.render(vvx,vvy, false, shaders);
        }


        engine->go2D();

        // Render 2D stuff here
        sf::Texture::bind(Assets::GetImage("crosshair"));
        int hw = global_config.width/2;
        int hh = global_config.height/2;
        glColor4f(1,1,1,1);
        glBegin(GL_QUADS);
            glTexCoord2f(0.0F,0.0F); glVertex2i(hw-8,hh-8);
            glTexCoord2f(1.0F,0.0F); glVertex2i(hw+8,hh-8);
            glTexCoord2f(1.0F,1.0F); glVertex2i(hw+8,hh+8);
            glTexCoord2f(0.0F,1.0F); glVertex2i(hw-8,hh+8);
        glEnd();

        engine->Render();
    }

    delete engine;

    delete font_game;
    Assets::Destroy();

    global_config.Write();

    lout.flush();
    lout.close();

    return 0;
}

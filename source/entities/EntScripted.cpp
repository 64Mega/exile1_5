
#include <entities/EntScripted.h>
#include <spectrum/spectrum.h>
#include <spectrum/engine/sprite.h>
#include <player.h>
#include <gl/gl.h>

// Include APIS

#include <gl_api.h>
#include <sfml_api.h>
#include <engine_api.h>

using namespace std;

void ScriptedEntity::registerBinding() {

    SLB::Class<Engine>("Game::Engine")
    // .constructor()
    .set("Print",(void (*)(int, int, string, float, float, float)) &Engine::Print)
    .set("SetSTMessage",(void (*)(string, int, bool, bool)) &Engine::SetSTMessage);

    SLB::Class<ScriptedEntity>("Game::SEntity")
    .constructor()
    .set("ResetView",(int (*)()) &ScriptedEntity::PlayerResetView)
    .set("Go3D",(void (*)()) &ScriptedEntity::EngineGo3D)
    .set("Go2D",(void (*)(bool)) &ScriptedEntity::EngineGo2D);


    // GL binding
    API_GL::bind();

    // SFML binding
    API_SFML::bind();

    // Engine bindings
    API_ENGINE::bind();

}

int ScriptedEntity::GetNextID()
{
    return Entity::g_id++;
}

// Constructor. Sets up the LUA state.

ScriptedEntity::ScriptedEntity() {
    id = -1;
    script = "";

    L = luaL_newstate();
    luaL_openlibs(L);

    SLB::Manager::defaultManager()->registerSLB(L);
}

void ScriptedEntity::Setup(std::string script, int id) {
    this->script = script;

    this->id = id;
    cout << "SETUP" << endl;

    luaL_dofile(L, script.c_str());
}

ScriptedEntity& ScriptedEntity::copy() {
    ScriptedEntity* pcopy = new ScriptedEntity(*this);
    return *pcopy;
}

void ScriptedEntity::spawn(float x, float y, float z, int meta)
{
    Entity::spawn(x,y,z,meta);

    // Register handles
    registerBinding();

    cout << "SPAWNING SCRIPT" << endl;

    luaL_dofile(L, script.c_str());

    lua_getglobal(L, "init");
    lua_call(L,0,0);
}

void ScriptedEntity::update()
{
    lua_getglobal(L, "update");
    lua_call(L,0,1);

    int i = (int)lua_tointeger(L, -1);
    dead = (bool)i;

    if(Player::getPlayer())Player::getPlayer()->resetView();
}

ScriptedEntity::~ScriptedEntity() {

    if(script != "")
    {
        lua_getglobal(L, "destroy");
        lua_call(L,0,0);
    }

    if(L)
    {
        lua_close(L);
    }
}

// Some API related functionality
void ScriptedEntity::PlayerResetView()
{
    if(Player::getPlayer())
    {
        Player::getPlayer()->resetView();
    }
}

void ScriptedEntity::EngineGo2D(bool doublescale)
{
    if(doublescale)
        Engine::GetEngine().go2DX();
    else
        Engine::GetEngine().go2D();
}

void ScriptedEntity::EngineGo3D()
{
    Engine::GetEngine().go3D();
}


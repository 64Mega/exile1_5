
#include <editor/exitscreen.h>
#include <editor/save.h>
#include <spectrum/spectrum.h>
#include <input.h>

ExitScreen::ExitScreen()
{
    parent = NULL;
    done = false;
    screenName = "Exit Screen";
    cout << "[SCREEN] Entered Exit Screen..." << endl;
}

ExitScreen::ExitScreen(ScreenEditor* p)
{
    parent = p;
    done = false;
    screenName = "Exit Screen";
    cout << "[SCREEN] Entered Exit Screen..." << endl;
}

ExitScreen::~ExitScreen()
{
    cout << "Leaving Exit Screen..." << endl;
}

void ExitScreen::update()
{
    Engine& engine = Engine::GetEngine();
    engine.go2DX();

    engine.Print(56,100,"SAVE BEFORE EXITING? (Y/N)");

    if(Input::keyPressed(sf::Keyboard::N))
    {
        parent->canExit = true;
        done = true;
        return;
    }
    if(Input::keyPressed(sf::Keyboard::Y))
    {
        done = true;
        parent->canExit = true;
        parent->screenlist.push_back(new SaveScreen(parent));
        return;
    }

    engine.Render();
}

using namespace std;



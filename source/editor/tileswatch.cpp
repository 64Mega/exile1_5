// TileSwatch implementation

#include <editor/tileswatch.h>
#include <string>
#include <iostream>

#include <spectrum/spectrum.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include <input.h>

using namespace std;

TileSwatch::TileSwatch()
{
    parent = NULL;
    done = false;
    screenName = "Tile Swatch";
    cout << "[SCREEN] Entered Tile Swatch" << endl;
    tex_cursor.loadFromFile("./editor/cursor.png");
}

TileSwatch::TileSwatch(ScreenEditor* editor)
{
    parent = editor;
    done = false;
    screenName = "Tile Swatch";
    cout << "[SCREEN] Entered Tile Swatch" << endl;
    tex_cursor.loadFromFile("./editor/cursor.png");
}

TileSwatch::~TileSwatch()
{
    cout << "[SCREEN] Exited Tile Swatch." << endl;
    parent = NULL;
}

void TileSwatch::update()
{
    static int page = 0;

    Engine& engine = Engine::GetEngine();
    engine.go2D();
    engine.Print(0,0,"TILE SWATCH");

    // Get mouse coordinates
    int mx = sf::Mouse::getPosition(engine.GetRenderContext()).x;
    int my = sf::Mouse::getPosition(engine.GetRenderContext()).y;

    if(Input::keyPressed(sf::Keyboard::Escape))
    {
        done = true;
    }

    int numtiles = Assets::GetNumTiles();

    if(Input::keyPressed(sf::Keyboard::Right))
    {
        if(page < numtiles/96)page += 1;
    }
    if(Input::keyPressed(sf::Keyboard::Left))
    {
        if(page > 0)page -= 1;
    }

    for(int i = page*96; i < (page*96)+96; i++)
    {
        if(i >= numtiles) break;
        int mix = i%8;
        int miy = (i/8);

        int ix = mix*72;
        int iy = (miy*72)+16;

        bool mouseover = !(mx < ix || mx > ix+72 || my < iy || my > iy+72);

        if(mouseover)
        {
            glColor3f(0.0F,0.8F,0.8F);
            glBegin(GL_LINES);
                glVertex2i(ix+1,iy);
                glVertex2i(ix+71,iy);

                glVertex2i(ix+71,iy);
                glVertex2i(ix+71,iy+70);

                glVertex2i(ix+71,iy+70);
                glVertex2i(ix,iy+70);

                glVertex2i(ix,iy+70);
                glVertex2i(ix,iy);
            glEnd();

            if(Input::click(sf::Mouse::Left))
            {
                parent->c_tile = i;
                done = true;
            }
        }

        sf::Texture::bind(Assets::GetTileByIndex(i));
        glColor3f(1,1,1);
        glBegin(GL_QUADS);
            glTexCoord2i(0,0); glVertex2i(ix+4,iy+8);
            glTexCoord2i(1,0); glVertex2i(ix+68,iy+8);
            glTexCoord2i(1,1); glVertex2i(ix+68,iy+72);
            glTexCoord2i(0,1); glVertex2i(ix+4,iy+72);
        glEnd();

        sf::Texture::bind(NULL);

        if(mouseover)engine.Print(ix,iy,Assets::GetTileName(i),0.4,0.9,0.4);
        else engine.Print(ix,iy,Assets::GetTileName(i),0.4,0.4,0.4);
    }

    sf::Texture::bind(&tex_cursor);
    glColor3f(1,1,1);
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex2i(mx,my);
        glTexCoord2f(1,0); glVertex2i(mx+16,my);
        glTexCoord2f(1,1); glVertex2i(mx+16,my+16);
        glTexCoord2f(0,1); glVertex2i(mx,my+16);
    glEnd();

    engine.Render();
}

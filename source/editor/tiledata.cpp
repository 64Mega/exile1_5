// Basic initializer

#include <editor/tiledata.h>

T_ETile::T_ETile()
{
    meta = 0;
    tileindex = -1;
    height = 0;
}

T_EWall::T_EWall()
{
    meta = 0;
    type = 0;
    tileindex = -1;
}

T_EEntity::T_EEntity()
{
    meta = 0;
    type = -1;
    height = 0;
}

T_ELight::T_ELight()
{
    level = 0;
}



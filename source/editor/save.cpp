
#include <editor/save.h>
#include <sfml/graphics.hpp>
#include <gl/gl.h>
#include <gl/glu.h>
#include <cstdio>
#include <string>
#include <iostream>

#include <spectrum/spectrum.h>

using namespace std;

SaveScreen::SaveScreen()
{
    parent = NULL;
    done = false;
    screenName = "SaveScreen";
    cout << "[SCREEN] SaveScreen entered." << endl;
}

SaveScreen::SaveScreen(ScreenEditor* p)
{
    parent = p;
    done = false;
    screenName = "SaveScreen";
    cout << "[SCREEN] SaveScreen entered." << endl;
}

SaveScreen::~SaveScreen()
{
    parent = NULL;
    cout << "[SCREEN] Exited SaveScreen." << endl;
}

void SaveScreen::update()
{
    Engine& engine = Engine::GetEngine();
    engine.go2DX();

    string buffer = parent->mapname;
    bool finished = false;
    char c;
    sf::Event ev;
    while(!finished)
    {
    while(engine.GetRenderContext().pollEvent(ev))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        engine.Print(0,0,"SAVE MAP:");
        engine.Print(0,10,"> ");

        if(ev.type == sf::Event::KeyPressed)
        {
            if(ev.key.code <= 25)
            {
                if(ev.key.shift) buffer += ev.key.code + 65;
                else buffer += ev.key.code + 65;
            }
            if(ev.key.code > 25 && ev.key.code <= 35)
            {
                buffer += ev.key.code-26 + 48;
            }
            if(ev.key.code == sf::Keyboard::Period)
            {
                buffer += '.';
            }
            if(ev.key.code == sf::Keyboard::BackSpace)
            {
                buffer = buffer.substr(0,buffer.size()-1);
            }
            if(ev.key.code == sf::Keyboard::Return)
            {
                if(buffer.empty() != true)
                {
                    parent->save(buffer);
                    done = true;
                    finished = true;
                }
            }
            engine.Print(16,10,buffer);
        }
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
        {
            finished = true;
            done = true;
        }
        engine.Print(16,10,buffer);
        engine.Render();
    }
    }

    engine.Render();
}

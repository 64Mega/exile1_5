// EEntSwatch implementation

#include <editor/EEntswatch.h>
#include <string>
#include <iostream>

#include <spectrum/spectrum.h>
#include <gl/gl.h>
#include <gl/glu.h>

#include <input.h>

using namespace std;

EEntSwatch::EEntSwatch()
{
    parent = NULL;
    done = false;
    screenName = "EEnt Swatch";
    cout << "[SCREEN] Entered EEnt Swatch" << endl;
    tex_cursor.loadFromFile("./editor/cursor.png");
}

EEntSwatch::EEntSwatch(ScreenEditor* editor)
{
    parent = editor;
    done = false;
    screenName = "EEnt Swatch";
    cout << "[SCREEN] Entered EEnt Swatch" << endl;
    tex_cursor.loadFromFile("./editor/cursor.png");
}

EEntSwatch::~EEntSwatch()
{
    cout << "[SCREEN] Exited EEnt Swatch." << endl;
    parent = NULL;
}

void EEntSwatch::update()
{
    Engine& engine = Engine::GetEngine();
    engine.go2D();
    engine.Print(0,0,"EEnt SWATCH");

    // Get mouse coordinates
    int mx = sf::Mouse::getPosition(engine.GetRenderContext()).x;
    int my = sf::Mouse::getPosition(engine.GetRenderContext()).y;

    if(Input::keyPressed(sf::Keyboard::Escape))
    {
        done = true;
    }

    int numEEnts = Assets::GetNumEEnts();
    int page = 0;

    for(int i = page*192; i < (page*192)+192; i++)
    {
        if(i >= numEEnts) break;
        int mix = i%16;
        int miy = (i/16);

        int ix = mix*40;
        int iy = (miy*40)+16;

        bool mouseover = !(mx < ix || mx > ix+40 || my < iy || my > iy+40);

        if(mouseover)
        {
            glColor3f(0.0F,0.8F,0.8F);
            glBegin(GL_LINES);
                glVertex2i(ix+1,iy);
                glVertex2i(ix+41,iy);

                glVertex2i(ix+41,iy);
                glVertex2i(ix+41,iy+40);

                glVertex2i(ix+41,iy+40);
                glVertex2i(ix,iy+40);

                glVertex2i(ix,iy+40);
                glVertex2i(ix,iy);
            glEnd();

            if(Input::click(sf::Mouse::Left))
            {
                parent->c_eent = i;
                parent->mode = Mode_Ents;
                done = true;
            }
        }

        sf::Texture::bind(Assets::GetEEntByIndex(i));
        glColor3f(1,1,1);
        glBegin(GL_QUADS);
            glTexCoord2i(0,0); glVertex2i(ix+4,iy+8);
            glTexCoord2i(1,0); glVertex2i(ix+36,iy+8);
            glTexCoord2i(1,1); glVertex2i(ix+36,iy+40);
            glTexCoord2i(0,1); glVertex2i(ix+4,iy+40);
        glEnd();

        sf::Texture::bind(NULL);

        if(mouseover)engine.Print(ix,iy,Assets::GetEEntName(i),0.4,0.9,0.4);
        else engine.Print(ix,iy,Assets::GetEEntName(i),0.4,0.4,0.4);
    }

    sf::Texture::bind(&tex_cursor);
    glColor3f(1,1,1);
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex2i(mx,my);
        glTexCoord2f(1,0); glVertex2i(mx+16,my);
        glTexCoord2f(1,1); glVertex2i(mx+16,my+16);
        glTexCoord2f(0,1); glVertex2i(mx,my+16);
    glEnd();

    engine.Render();
}



#include <player.h>
#include <map.h>

#include <gl/gl.h>
#include <gl/glu.h>

#include <editor/edit3d.h>
#include <mouselook.h>
#include <input.h>
#include <config.h>

#define MOVESPEED 0.75F

Player::Player():Entity()
{
    id = Entity::g_id++; // 0 is the player ID.

    pos = MXVector(0.0F,0.0F,0.0F);
    rot = MXVector(0.0F,0.0F,0.0F);
    Player::c_player = this;
    crouch = 1.5F;
    boboffset = 0.0F;
    bobtheta = 0.0F;

    pvx = pvy = pvz = 0.0F;

    lightsource = 0;

    MouseLook::init();
}

Player::Player(MXVector pos, float rotation):Player()
{
    // Player();
    this->pos = pos;
    Player::c_player = this;
    rot = MXVector(0.0F, rotation, 0.0F);
}

Player::Player(const Player& p):Player()
{
    pos = p.pos;
    crouch = p.crouch;
    rot = p.rot;
    boboffset = p.boboffset;
    bobtheta = p.bobtheta;

    pvx = p.pvx;
    pvy = p.pvy;
    pvz = p.pvz;
    lightsource = p.lightsource;

    c_player = this;
}

Player::~Player()
{
    if(Player::c_player == this)Player::c_player = NULL;
}

Player* Player::c_player = NULL;

Player* Player::getPlayer()
{
    return Player::c_player;
}

void Player::setYRot(float ry)
{
    this->rot.y = ry;
}

void Player::update()
{
    // Draw test hands
    Engine::GetEngine().go2D();
    sf::Texture::bind(Assets::GetImage("hand1"));
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER,0);
    //glEnable(GL_BLEND);
    //glDisable(GL_DEPTH);

    int h = global_config.height;
    int w = global_config.width;
    int hww = (w/2)-(w/4);

    float bl = 1.0-Map::getMap()->GetNearestLightLevel(pos.x,pos.z,1.0F);

    glColor3f(bl,bl,bl);
    if(lightsource > 0)
    {
        glColor3f(1.0F,0.9F,0.9F);
    }
    glBegin(GL_QUADS);
        glTexCoord2f(0,0); glVertex2i(hww+32,h+20-192+boboffset*700);
        glTexCoord2f(1,0); glVertex2i(hww+192+32,h+20-192+boboffset*700);
        glTexCoord2f(1,1); glVertex2i(hww+192+32,h+20+boboffset*700);
        glTexCoord2f(0,1); glVertex2i(hww+32,h+20+boboffset*700);
    glEnd();
    glBegin(GL_QUADS);
        glTexCoord2f(1,0); glVertex2i(w-hww-192-32,h+20-192+boboffset*700);
        glTexCoord2f(0,0); glVertex2i(w-hww-32,h+20-192+boboffset*700);
        glTexCoord2f(0,1); glVertex2i(w-hww-32,h+20+boboffset*700);
        glTexCoord2f(1,1); glVertex2i(w-hww-192-32,h+20+boboffset*700);
    glEnd();
    //glDisable(GL_BLEND);
    glDisable(GL_ALPHA_TEST);
   // glEnable(GL_DEPTH);
    Engine::GetEngine().go3D();

    // Set player viewpoint, do mouse delta calculations and smoothing, etc.

    static bool bobFlip = false;

    float move_velocity = 0.0F;
    float strafe_velocity = 0.0F;
    float lookh_velocity = 0.0F;
    float lookw_velocity = 0.0F;

    // Light toggle for debug
    if(Input::keyPressed(sf::Keyboard::Tab))
    {
        if(lightsource == 0) lightsource = 1;
        else lightsource = 0;
    }

    MouseLook::update();
    rot.y += MouseLook::deltax * MOUSE_STRENGTH;
    rot.x -= MouseLook::deltay * MOUSE_STRENGTH;

    // Gamepad look.
    float gpdeltax = -(Input::axisR()/100.0F);
    float gpdeltay = -(Input::axisU()/100.0F);
    rot.y -= gpdeltay * MOUSE_STRENGTH/4;
    rot.x += gpdeltax * MOUSE_STRENGTH/4;

    if(rot.x < -89.0F) rot.x = -89.0F;
    if(rot.x > 89.0F) rot.x = 89.0F;

    float vx = cos(DEGTORADS(rot.y))*MOVESPEED;
    float vz = sin(DEGTORADS(rot.y))*MOVESPEED;

    Map::getMap()->setPlayerLight(pos.x+vx*2.0,pos.z+vz*2.0,pos.y,lightsource);

    float sryl = rot.y - 90.0F;
    while(sryl < 0.0F) sryl += 360.0F;
    while(sryl > 359.0F) sryl -= 360.0F;

    // Reset bob when not moving
    if(!Input::keyDown(sf::Keyboard::W) &&
       !Input::keyDown(sf::Keyboard::S) &&
       !Input::keyDown(sf::Keyboard::A) &&
       !Input::keyDown(sf::Keyboard::D) &&
       Input::axisX() == 0.0F &&
       Input::axisY() == 0.0F)
    {
        if(boboffset < -0.04F) boboffset += 0.02F;
        else if(boboffset > 0.04F) boboffset -=0.02F;
        else
        {
            boboffset = 0.0F;
            bobtheta = 0.0F;
        }
    }
    else
    {
        bobtheta += 0.15F;
        float bobraw = sinf(bobtheta);
        boboffset = bobraw/75.0F;
        Assets::GetSound("player_footstep")->setVolume(20.0F);
        if(crouch < 1.0F) Assets::GetSound("player_footstep")->setVolume(10.0F);
        float rpitch = (rand()%100)/100.0F;
        Assets::GetSound("player_footstep")->setPitch(0.7F+rpitch);

        if(bobFlip && bobraw >= 0.99F)
        {
            bobFlip = false;
            //Assets::PlaySound("player_footstep");
        }
        else
        if(!bobFlip && bobraw <= -0.99F)
        {
            bobFlip = true;
            Assets::PlaySound("player_footstep");
        }
    }

    // Steal the movement code from Edit3D

    float vxr = cos(DEGTORADS(sryl));
    float vzr = sin(DEGTORADS(sryl));

    float camheight = 0.0F;
    int indexp = (int)(floor(pos.z)*MAP_SIZE+floor(pos.x));

    if(indexp >= 0 && indexp < MAP_SIZE*MAP_SIZE)
    {
        if(Map::getMap()->floors()[indexp].tileindex >= 0)
        {
            float hd = abs(pos.y - (Map::getMap()->floors()[indexp].height/8.0F));
            if(pos.y < Map::getMap()->floors()[indexp].height/8.0F && hd <= 0.8F)pos.y += 0.1F;
            if(pos.y > Map::getMap()->floors()[indexp].height/8.0F)pos.y -= 0.1F;
        }
    }

    gluLookAt(pos.x, pos.y+crouch+boboffset, pos.z, pos.x+vx, pos.y+crouch+boboffset+tan(DEGTORADS(rot.x)), pos.z+vz,0,1,0);
    // Set public view vector
    pvx = vx;
    pvy = tan(DEGTORADS(rot.x));
    pvz = vz;

    // Crouching
    if(Input::keyDown(sf::Keyboard::C))
    {
        if(crouch > 0.7F)
        {
            crouch -= 0.1F;
        }
        else crouch = 0.7F;
    }
    else
    {
        int indexx = (int)(floor(pos.z)*MAP_SIZE+floor(pos.x+vx*0.5F));
        int indexz = (int)(floor(pos.z+vz*0.5F)*MAP_SIZE+floor(pos.x));
        bool nx = pos.y > Map::getMap()->floors()[indexx].height/8.0F;
        bool nz = pos.y > Map::getMap()->floors()[indexz].height/8.0F;

        float hhx = abs(Map::getMap()->floors()[indexx].height/8.0F-Map::getMap()->ceils()[indexx].height/8.0F);
        float hhz = abs(Map::getMap()->floors()[indexz].height/8.0F-Map::getMap()->ceils()[indexz].height/8.0F);

        bool ox = true;
        bool oz = true;
        if(hhx > (1/crouch)+0.5F)ox = false;
        if(hhz > (1/crouch)+0.5F)oz = false;
        if(crouch < 1.5F)
        {
            if(ox && oz)crouch += 0.1F;
        }
        else crouch = 1.5F;
    }

    // Gamepad movement. Very simple.
    move_velocity = -(Input::axisY()/100.0F);
    strafe_velocity = -(Input::axisX()/100.0F);

    // Movement
    if(Input::keyDown(sf::Keyboard::W))
    {
        move_velocity = 1.0F;
    }
    if(Input::keyDown(sf::Keyboard::S))
    {
        move_velocity = -1.0F;
    }
    if(Input::keyDown(sf::Keyboard::A))
    {
        strafe_velocity = 1.0F;
    }
    if(Input::keyDown(sf::Keyboard::D))
    {
        strafe_velocity = -1.0F;
    }

    vx = vx*move_velocity;
    vz = vz*move_velocity;
    vxr = vxr*strafe_velocity*0.75;
    vzr = vzr*strafe_velocity*0.75;

    if(move_velocity != 0.0F)
    {
        int indexx = (int)(floor(pos.z)*MAP_SIZE+floor(pos.x+vx*0.5F));
        int indexz = (int)(floor(pos.z+vz*0.5F)*MAP_SIZE+floor(pos.x));
        bool nx = pos.y > Map::getMap()->floors()[indexx].height/8.0F;
        bool nz = pos.y > Map::getMap()->floors()[indexz].height/8.0F;

        float hhx = abs(Map::getMap()->floors()[indexx].height/8.0F-Map::getMap()->ceils()[indexx].height/8.0F);
        float hhz = abs(Map::getMap()->floors()[indexz].height/8.0F-Map::getMap()->ceils()[indexz].height/8.0F);

        bool ox = true;
        bool oz = true;
        if(hhx > (1/crouch)+1)ox = false;
        if(hhz > (1/crouch)+1)oz = false;

        float hdx = abs(pos.y - (Map::getMap()->floors()[indexx].height/8.0F));
        float hdz = abs(pos.y - (Map::getMap()->floors()[indexz].height/8.0F));

        bool colx = Map::getMap()->cellCollision(pos.x,pos.z,pos.x+(vx*0.5F),pos.z);
        bool colz = Map::getMap()->cellCollision(pos.x,pos.z,pos.x,pos.z+(vz*0.5F));
        if(!colx && ox && (hdx <= 0.8F || (nx)))pos.x += vx*0.05;
        if(!colz && oz && (hdz <= 0.8F || (nz)))pos.z += vz*0.05;
    }

    if(strafe_velocity != 0.0F)
    {
        int indexx = (int)(floor(pos.z)*MAP_SIZE+floor(pos.x+vxr*0.5F));
        int indexz = (int)(floor(pos.z+vzr*0.5F)*MAP_SIZE+floor(pos.x));
        bool nx = pos.y > Map::getMap()->floors()[indexx].height/8.0F;
        bool nz = pos.y > Map::getMap()->floors()[indexz].height/8.0F;
        float hdx = abs(pos.y - (Map::getMap()->floors()[indexx].height/8.0F));
        float hdz = abs(pos.y - (Map::getMap()->floors()[indexz].height/8.0F));

        float hhx = abs(Map::getMap()->floors()[indexx].height/8.0F-Map::getMap()->ceils()[indexx].height/8.0F);
        float hhz = abs(Map::getMap()->floors()[indexz].height/8.0F-Map::getMap()->ceils()[indexz].height/8.0F);

        bool ox = true;
        bool oz = true;
        if(hhx > 2.0F)ox = false;
        if(hhz > 2.0F)oz = false;

        bool colx = Map::getMap()->cellCollision(pos.x,pos.z,pos.x+(vxr*0.5F),pos.z);
        bool colz = Map::getMap()->cellCollision(pos.x,pos.z,pos.x,pos.z+(vzr*0.5F));
        if(!colx && ox && (hdx <= 0.8F || nx))pos.x += vxr*0.05;
        if(!colz && oz && (hdz <= 0.8F || nz))pos.z += vzr*0.05;
    }
}

// Copy code
Player& Player::copy()
{
    Player* pcopy = new Player(*this);
    return *pcopy;
}

// Spawn handler
void Player::spawn(float x, float y, float z, int meta)
{
    Entity::spawn(x,y,z,meta);
    MouseLook::init();
    rot.y = 10.0F + (float)meta;
}

void Player::resetView()
{
    float vx = cos(DEGTORADS(rot.y))*MOVESPEED;
    float vz = sin(DEGTORADS(rot.y))*MOVESPEED;
    gluLookAt(pos.x, pos.y+crouch+boboffset, pos.z, pos.x+vx, pos.y+crouch+boboffset+tan(DEGTORADS(rot.x)), pos.z+vz,0,1,0);
}

void Player::setCustomView(float rx, float ry)
{
    if(rot.x < -89.0F) rot.x = -89.0F;
    if(rot.x > 89.0F) rot.x = 89.0F;

    float vx = cos(DEGTORADS(rot.y))*MOVESPEED;
    float vz = sin(DEGTORADS(rot.y))*MOVESPEED;
    gluLookAt(pos.x, pos.y+crouch+boboffset, pos.z, pos.x+vx, pos.y+crouch+boboffset+tan(DEGTORADS(rot.x)), pos.z+vz,0,1,0);
}

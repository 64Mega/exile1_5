// Line/Line collision methods

#include <linecol.h>
#include <vector>
#include <cmath>

using namespace std;


t_line::t_line(float x1, float y1, float x2, float y2)
{
    ax = x1;
    ay = y1;
    bx = x2;
    by = y2;
}

t_point::t_point(float ax, float ay)
{
    x = ax;
    y = ay;
}


std::vector<float> overlap_intervals(float ub1, float ub2)
{
    float l = fmin(ub1,ub2);
    float r = fmax(ub1,ub2);
    float A = fmax(0,l);
    float B = fmin(1,r);

    vector<float> tmp;

    if(A>B) return tmp; // No intersection
    else if(A == B)
    {
        tmp.push_back(A);
        return tmp;
    }
    else
    {
        tmp.push_back(A);
        tmp.push_back(B);
        return tmp;
    }
}

std::vector<t_point> simple_intersect(t_line a, t_line b)
{
    double MyEpsilon = 0.00001;

    vector<t_point> ret;

    if(a.ax == a.bx && a.ay == a.by)return ret;
    if(b.ax == b.bx && b.ay == b.by)return ret;

    float ua_t = (b.bx - b.ax) * (a.ay - b.ay) - (b.by - b.ay) * (a.ax - b.ax);
    float ub_t = (a.bx - a.ax) * (a.ay - b.ay) - (a.by - a.ay) * (a.ax - b.ax);
    float u_b = (b.by - b.ay) * (a.bx - a.ax) - (b.bx - b.ax) * (a.by - a.ay);

    if(!(-MyEpsilon < u_b && u_b < MyEpsilon))
    {
        float ua = ua_t / u_b;
        float ub = ub_t / u_b;
        if(0.0F <= ua && ua <= 1.0F && 0.0F <= ub && ub <= 1.0F)
        {
            t_point t(a.ax + ua * (a.bx-a.ax), a.ay + ua * (a.by-a.ay));
            ret.push_back(t);
            return ret;
        }
        else
        {
            return ret;
        }
    }

    return ret;
}

std::vector<t_point> line_oned_intersect(t_line a, t_line b)
{
    float ub1, ub2;
    vector<t_point> res;

    float denomx = a.bx - a.ax;
    float denomy = a.by - a.ay;

    if(fabs(denomx) > fabs(denomy))
    {
        ub1 = (b.ax - a.ax);
        ub2 = (b.bx - a.ax);
    }
    else
    {
        ub1 = (b.ay - a.ay)/denomy;
        ub2 = (b.by - a.ay)/denomy;
    }

    vector<float> interval = overlap_intervals(ub1,ub2);

    vector<float>::iterator i;
    for(i = interval.begin(); i != interval.end(); i++)
    {
        float x = a.bx * (*i) + a.ax * (1.0F - (*i));
        float y = a.by * (*i) + a.ay * (1.0F - (*i));
        t_point t(x,y);
        res.push_back(t);
    }

    return res;
}

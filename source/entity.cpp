// Implementation of Entity class
// --

#include <entity.h>
#include <spectrum/spectrum.h>

Entity::Entity()
{
    pos = MXVector(0.0F,0.0F,0.0F);
    rot = MXVector(0.0F,0.0F,0.0F);

    dead = false; // Flags entity for deletion
    id = -1; // -1 is the 'null' entity
    meta = 0;
}

int Entity::g_id = 0;

Entity::Entity(const Entity& ent)
{
    Entity();
    // Copy necessary data across
    pos = ent.pos;
    rot = ent.rot;
    dead = ent.dead;
    id = ent.id;
    meta = ent.meta;
}

int Entity::ID()
{
    return id;
}

Entity::Entity(MXVector apos)
{
    Entity();

    pos = apos;
}

Entity::~Entity()
{

}

void Entity::update()
{

}

// Other methods

Entity& Entity::setPos(MXVector apos)
{
    pos = apos;
    return *this;
}

Entity& Entity::setRot(MXVector arot)
{
    rot = arot;
    return *this;
}

void Entity::spawn(float x, float y, float z, int meta)
{
    this->pos.x = x;
    this->pos.y = y;
    this->pos.z = z;
    this->meta = meta;
}

sf::Texture* getIcon() {
    return NULL;
}

Entity& Entity::copy()
{
    Entity* tcopy = new Entity(*this);
    return *tcopy;
}



#include <map.h>

#include <spectrum/spectrum.h>
#include <cstdio>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <gl/gl.h>
#include <gl/glu.h>

#include <player.h>
#include <linecol.h>

using namespace std;

Map* Map::c_map = NULL;

Map::Map()
{
    Map::c_map = this;
    map_loaded = false;

    d_floors = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_ceils = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_walls = new std::vector<T_EWall>(MAP_SIZE*MAP_SIZE);
    d_ents = new std::vector<T_EEntity>(MAP_SIZE*MAP_SIZE);
    d_lights = new std::vector<T_ELight>(MAP_SIZE*MAP_SIZE);

    MapLight aplight;
    aplight.x = 0;
    aplight.y = 0;
    aplight.z = 0;
    aplight.type = 1;
    mlights.push_back(aplight);

    floorpiece = 0;
    wallpiece = 0;
    ceilpiece = 0;

    name = "";

    // Load shaders
    mapshader.loadFromFile("shaders/hemilight.vert","shaders/hemilight.frag");
}

Map::~Map()
{
    delete d_floors;
    delete d_ceils;
    delete d_walls;
    delete d_ents;
    delete d_lights;
    Map::c_map = NULL;

    cout << "Destroyed map." << endl;
}

Map* Map::getMap()
{
    return Map::c_map;
}

void Map::clear()
{
    delete d_floors;
    delete d_ceils;
    delete d_walls;
    delete d_ents;
    delete d_lights;
    map_loaded = false;

    d_floors = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_ceils = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    d_walls = new std::vector<T_EWall>(MAP_SIZE*MAP_SIZE);
    d_ents = new std::vector<T_EEntity>(MAP_SIZE*MAP_SIZE);
    d_lights = new std::vector<T_ELight>(MAP_SIZE*MAP_SIZE);

	// Delete tables
	while(t_tiles.size() > 0) 
	{
		t_tiles.pop_back();
	}
	while(i_tiles.size() > 0)
	{
		i_tiles.pop_back();
	}
}

void Map::load(string filename)
{
    string mapname = "./maps/" + filename + ".MAP";
    name = filename;
    map_loaded = false;
    FILE* fin = fopen(mapname.c_str(),"rb"); // Derp.
    if(!fin)
    {
        cout << "[MAP] Error opening '" + mapname + "' for loading!" << endl;
        return;
    }

    clear();

	int dimensions = MAP_SIZE;
	char* mapid = new char[4];
	fread(mapid, 3, sizeof(char), fin);
	mapid[3] = '\0';

	printf("[Map] MapID: %s\n", mapid);

	if(string(mapid) != "MAP")
	{
		printf("[Map] MapID != \"MAP\"!");
		return;
	}
	delete[] mapid;

	fread(&dimensions, 1, sizeof(int), fin);
	
	// Read table
	int tableLen = 0;
	fread(&tableLen, 1, sizeof(int), fin);

	printf("[Map] Table entry length: %d\n", tableLen);

	for(int i = 0; i < tableLen; i++)
	{
		int slen;
		char* sbuf;

		fread(&slen, 1, sizeof(int), fin);
		sbuf = new char[slen+1];
		fread(sbuf, slen, sizeof(char), fin);
		sbuf[slen] = '\0';

		// Do lookup now to save time.
		t_tiles.push_back(sbuf);
		i_tiles.push_back(Assets::GetTileNumber(sbuf));
		
		delete[] sbuf;
	}
	

    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_floors)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_floors)[i].height,1,sizeof(int),fin);
        fread(&(*d_floors)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_walls)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_walls)[i].type,1,sizeof(int),fin);
        fread(&(*d_walls)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_ceils)[i].tileindex,1,sizeof(int),fin);
        fread(&(*d_ceils)[i].height,1,sizeof(int),fin);
        fread(&(*d_ceils)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_ents)[i].type,1,sizeof(int),fin);
        fread(&(*d_ents)[i].height,1,sizeof(int),fin);
        fread(&(*d_ents)[i].meta,1,sizeof(int),fin);
    }
    for(int i = 0; i < MAP_SIZE*MAP_SIZE; i++)
    {
        fread(&(*d_lights)[i].level,1,sizeof(int),fin);
        if((*d_lights)[i].level > 0)
        {
            MapLight m;
            m.x = i%MAP_SIZE;
            m.y = i/MAP_SIZE;
            m.z = 2;
            m.type = 1;
            mlights.push_back(m);
            cout << "ADDED LIGHT TO LEVEL AT (" << m.x << "," << m.y << "," << m.z << ")" << endl;
        }
    }

    map_loaded = true;

    fclose(fin);
}

MXVector Map::getPlayerStart() // Temporary. Going to use script tags for this.
{

    for(int iy = 0; iy < MAP_SIZE; iy++)
    for(int ix = 0; ix < MAP_SIZE; ix++)
    {
        int index = iy*MAP_SIZE+ix;
        if(ents()[index].type == 0)
        {
            return MXVector(ix,0.0F,iy);
        }
    }

    return MXVector(0.0F,0.0F,0.0F);
}

bool Map::cellCollision(float x1, float y1, float x2, float y2)
{
    t_line pline(x1,y1,x2,y2);

    int x = (int)round(x1);
    int y = (int)round(y1);

    int sx = x-2;
    int sy = y-2;
    int ex = x+2;
    int ey = y+2;

    if(sx < 0) sx = 0;
    if(sx > (MAP_SIZE*MAP_SIZE)-1)sx = (MAP_SIZE*MAP_SIZE)-1; // Derp
    if(sy < 0) sy = 0;
    if(sy > (MAP_SIZE*MAP_SIZE)-1)sy = (MAP_SIZE*MAP_SIZE)-1;
    if(ex < 0) ex = 0;
    if(ex > (MAP_SIZE*MAP_SIZE)-1)ex = (MAP_SIZE*MAP_SIZE)-1;
    if(ey < 0) ey = 0;
    if(ey > (MAP_SIZE*MAP_SIZE)-1)ey = (MAP_SIZE*MAP_SIZE)-1;

    for(int iy = sy; iy <= ey; iy++)
    {
        for(int ix = sx; ix <= ex; ix++)
        {
            if(walls()[iy*MAP_SIZE+ix].tileindex >= 0) // Derp
            {
                //cout << "index: " << iy*MAP_SIZE+ix << endl;
                t_line wline(0,0,0,0);
                int type = walls()[iy*MAP_SIZE+ix].type;
                if(type == 0) wline = t_line(ix,iy,ix,iy+1);
                if(type == 1) wline = t_line(ix+1,iy,ix+1,iy+1);
                if(type == 2) wline = t_line(ix,iy,ix+1,iy);
                if(type == 3) wline = t_line(ix,iy+1,ix+1,iy+1);
                if(type == 4) wline = t_line(ix,iy,ix+1,iy+1);
                if(type == 5) wline = t_line(ix+1,iy,ix,iy+1);

                if(simple_intersect(pline,wline).empty() != true)return true;
            }
        }
    }

    return false;
}

void Map::render(int posx, int posy, bool mode, bool shaders)
{
    // Select subsection of map to render
    //int sx = floor(posx/32)-10;
    //int sy = floor(posy/32)-10;
    int rsx = posx-20;
    int rsy = posy-20;
    int sx = posx-20;
    int sy = posy-20;
    int ex = posx+20;
    int ey = posy+20;

    if(sx < 0) sx = 0;
    if(sy < 0) sy = 0;
    if(ex > MAP_SIZE-1) ex = MAP_SIZE-1;
    if(ey > MAP_SIZE-1) ey = MAP_SIZE-1;

    bool hasFloor = false;

    floorpiece = -1;
    wallpiece = -1;
    ceilpiece = -1;

    if(shaders && !mode)
    {
        sf::Shader::bind(&mapshader);

        float lx = Player::getPlayer()->pos.x;
        float ly = Player::getPlayer()->pos.y;
        float lz = Player::getPlayer()->pos.z;

        mapshader.setParameter("LightPosition",lx,ly,lz);
        mapshader.setParameter("SkyColor",0.3F,0.3F,0.3F,1.0F);
        mapshader.setParameter("GroundColor",0.1F,0.1F,0.1F,1.0F);
        mapshader.setParameter("tex",sf::Shader::CurrentTexture);
    }

    float txo = 0.0F;
    float tyo = 0.0F;

    for(int iy = sy; iy <= ey; iy++)
    {
        for(int ix = sx; ix <= ex; ix++)
        {
            int index = iy*MAP_SIZE+ix;

            bool sideLeft = false;
            bool sideUp = false;

            // First render the floors
            glColor4f(1,1,1,1);
            float tx = (float)(ix);
            float ty = (float)(iy);

            // Calculate lighting for this tile
            float floorheight = (*d_floors)[index].height/8.0F;
            float ceilheight = 3.0F+(ceils()[index].height/8.0F);

            bool col = false;

            float px = 0.0f;
            float pz = 0.0f;
            float py = 0.0f;
            float ry = 0.0f;
            float rx = 0.0f;
            float vx = cos(DEGTORADS(ry))*3;
            float vz = sin(DEGTORADS(ry))*3;

            if(Player::getPlayer()) {
                px = Player::getPlayer()->pos.x;
                pz = Player::getPlayer()->pos.z;
                py = Player::getPlayer()->pos.y+1;
                ry = Player::getPlayer()->rot.y;
                rx = Player::getPlayer()->rot.x;
                vx = cos(DEGTORADS(ry))*3;
                vz = sin(DEGTORADS(ry))*3;
            }

            t_line pline(px,pz,px+vx,pz+vz);
            t_line wline(0,0,0,0);

            if((*d_floors)[index].tileindex >= 0)
            {
                // Try to do floor detection
                if(mode && !hasFloor)
                {
                    // Do a simple AABB test and check that RX is within range.
                    // This is terribly haxxy, but I'm still learning some of the
                    // more useful intersection tests (Ray/Triangle for instance).

                    float avz = sin(DEGTORADS(ry));
                    float avx = cos(DEGTORADS(ry));
                    float ptx1 = px+(avx*3);
                    float ptx2 = px+(avx*2);
                    float ptx3 = px+(avx*1);
                    float ptz1 = pz+(avz*3);
                    float ptz2 = pz+(avz*2);
                    float ptz3 = pz+(avz*1);
                    if(rx <= -10 && !(ptx1 < tx || ptx1 > tx+1 || ptz1 < ty || ptz1 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        floorpiece = index;
                    }
                    else if(rx <= -30 && !(ptx2 < tx || ptx2 > tx+1 || ptz2 < ty || ptz2 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        floorpiece = index;
                    }
                    else if(rx <= -50 && !(ptx3 < tx || ptx3 > tx+1 || ptz3 < ty || ptz3 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        floorpiece = index;
                    }
                }

                if(col)glColor3f(1,0.5,0.3);
                else glColor3f(1,1,1);

                sf::Texture::bind(Assets::GetTileByIndex(i_tiles[(*d_floors)[index].tileindex]));
                float fh = floors()[index].height/8.0F;
                float light = 1.0F;

                // mapshader.setParameter("nmap",*Assets::GetNMap(Assets::GetTileName((*d_floors)[index].tileindex)));
                glBegin(GL_QUADS);
                    glNormal3f(0,1.0F,0);
                    light = GetNearestLightLevel(ix,iy,0);
                    glTexCoord2f(0,0); glVertex4f(tx,fh,ty,light);
                    light = GetNearestLightLevel(ix+1,iy,0);
                    glTexCoord2f(1,0); glVertex4f(tx+1,fh,ty,light);
                    light = GetNearestLightLevel(ix+1,iy+1,0);
                    glTexCoord2f(1,1); glVertex4f(tx+1,fh,ty+1,light);
                    light = GetNearestLightLevel(ix,iy+1,0);
                    glTexCoord2f(0,1); glVertex4f(tx,fh,ty+1,light);
                glEnd();
            }
            if((*d_ceils)[index].tileindex >= 0)
            {
                // Ceiling hack
                if(mode && !hasFloor)
                {
                    float avz = sin(DEGTORADS(ry));
                    float avx = cos(DEGTORADS(ry));
                    float ptx1 = px+(avx*3);
                    float ptx2 = px+(avx*2);
                    float ptx3 = px+(avx*1);
                    float ptz1 = pz+(avz*3);
                    float ptz2 = pz+(avz*2);
                    float ptz3 = pz+(avz*1);
                    if(rx >= 30 && !(ptx1 < tx || ptx1 > tx+1 || ptz1 < ty || ptz1 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        ceilpiece = index;
                    }
                    else if(rx >= 50 && !(ptx2 < tx || ptx2 > tx+1 || ptz2 < ty || ptz2 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        ceilpiece = index;
                    }
                    else if(rx >= 70 && !(ptx3 < tx || ptx3 > tx+1 || ptz3 < ty || ptz3 > ty+1))
                    {
                        hasFloor = true;
                        col = true;
                        ceilpiece = index;
                    }
                }

                if(col)glColor3f(1,0.5,0.3);
                else glColor3f(1,1,1);

                float light = 1.0F;

                sf::Texture::bind(Assets::GetTileByIndex(i_tiles[(*d_ceils)[index].tileindex]));
                glBegin(GL_QUADS);
                    glNormal3f(0,-1,0);
                    light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(0,0); glVertex4f(tx,ceilheight,ty+1,light);
                    light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,0); glVertex4f(tx+1,ceilheight,ty+1,light);
                    light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(1,1); glVertex4f(tx+1,ceilheight,ty,light);
                    light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(0,1); glVertex4f(tx,ceilheight,ty,light);
                glEnd();
            }

            // Draw tile sides, one for each adjacent tile.
            int adjleft = index-1;
            int adjright = index+1;
            int adjup = index-MAP_SIZE;
            int adjdown = index+MAP_SIZE;

            if(adjleft >= 0)
            {
                // Do floors first
                if(floors()[adjleft].tileindex >= 0 && floors()[adjleft].height != floors()[index].height)
                {
                    sideLeft = true;
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[floors()[adjleft].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = floors()[index].height/8.0F;
                    float oh = floors()[adjleft].height/8.0F;
                    float texh = abs(th-oh); // Derp
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(-1,0,0);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx-0.001F,th,ty+1,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx-0.001F,th,ty,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,texh); glVertex4f(tx-0.001F,oh,ty,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(0,texh); glVertex4f(tx-0.001F,oh,ty+1,light);
                    glEnd();
                }
                // Ceilings next
                if(ceils()[adjleft].tileindex >= 0 && ceils()[adjleft].height != ceils()[index].height)
                {
                    sideLeft = true;
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[ceils()[adjleft].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = ceils()[index].height/8.0F;
                    float oh = ceils()[adjleft].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(1,0,0);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx-0.001F,th+3.0F,ty+1,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx-0.001F,th+3.0F,ty,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,texh); glVertex4f(tx-0.001F,oh+3.0F,ty,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(0,texh); glVertex4f(tx-0.001F,oh+3.0F,ty+1,light);
                    glEnd();
                }
            }

            if(adjright >= 0 && !sideLeft && adjleft >= 0)
            {
                // Do floors first
                if((floors()[adjright].tileindex >= 0 && floors()[adjleft].tileindex < 0)&& floors()[adjright].height != floors()[index].height)
                {
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[floors()[adjright].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = floors()[index].height/8.0F;
                    float oh = floors()[adjright].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(1,0,0);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1+0.001F,th,ty,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx+1+0.001F,th,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,texh); glVertex4f(tx+1+0.001F,oh,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(0,texh); glVertex4f(tx+1+0.001F,oh,ty,light);
                    glEnd();
                }
                // Ceilings next
                if((ceils()[adjright].tileindex >= 0 && ceils()[adjleft].tileindex < 0)&& ceils()[adjright].height != ceils()[index].height)
                {
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[ceils()[adjright].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = ceils()[index].height/8.0F;
                    float oh = ceils()[adjright].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(1,0,0);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1+0.001F,th+3.0F,ty,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx+1+0.001F,th+3.0F,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,texh); glVertex4f(tx+1+0.001F,oh+3.0F,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(0,texh); glVertex4f(tx+1+0.001F,oh+3.0F,ty,light);
                    glEnd();
                }
            }

            if(adjup >= 0)
            {

                // Do floors first
                if(floors()[adjup].tileindex >= 0 && floors()[adjup].height != floors()[index].height)
                {
                    sideUp = true;
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[floors()[adjup].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = floors()[index].height/8.0F;
                    float oh = floors()[adjup].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(0,0,-1);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1,th,ty-0.001F,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx,th,ty-0.001F,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,texh); glVertex4f(tx,oh,ty-0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(0,texh); glVertex4f(tx+1,oh,ty-0.001F,light);
                    glEnd();
                }
                // Ceilings next
                if(ceils()[adjup].tileindex >= 0 && ceils()[adjup].height != ceils()[index].height)
                {
                    sideUp = true;
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[ceils()[adjup].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = ceils()[index].height/8.0F;
                    float oh = ceils()[adjup].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(0,0,1);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx,th+3.0F,ty-0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx+1,th+3.0F,ty-0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(1,texh); glVertex4f(tx+1,oh+3.0F,ty-0.001F,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(0,texh); glVertex4f(tx,oh+3.0F,ty-0.001F,light);
                    glEnd();
                }
            }

            if(adjdown >= 0 && !sideUp && adjup >= 0)
            {
                // Do floors first
                if((floors()[adjdown].tileindex >= 0 && floors()[adjup].tileindex < 0) && floors()[adjdown].height != floors()[index].height)
                {
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[floors()[adjdown].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = floors()[index].height/8.0F;
                    float oh = floors()[adjdown].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(0,0,1);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx,th,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx+1,th,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,texh); glVertex4f(tx+1,oh,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(0,texh); glVertex4f(tx,oh,ty+1+0.001F,light);
                    glEnd();
                }
                // Ceilings next
                if((ceils()[adjdown].tileindex >= 0 && ceils()[adjup].tileindex < 0) && ceils()[adjdown].height != ceils()[index].height)
                {
                    // Use meta field to set texture for sides
                    sf::Texture::bind(Assets::GetTileByIndex(i_tiles[ceils()[adjdown].meta]));
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                    // Calculate height difference for texture coords
                    float th = ceils()[index].height/8.0F;
                    float oh = ceils()[adjdown].height/8.0F;
                    float texh = abs(th-oh);
                    float light = 1.0F;
                    glBegin(GL_QUADS);
                        glNormal3f(0,0,1);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1,th+3.0F,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx,th+3.0F,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(1,texh); glVertex4f(tx,oh+3.0F,ty+1+0.001F,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(0,texh); glVertex4f(tx+1,oh+3.0F,ty+1+0.001F,light);
                    glEnd();
                }
            }


            // Little collision test. Remove it afterwards.
            col = false;

            // Render walls
            if((*d_walls)[index].tileindex >= 0)
            {
                sf::Texture::bind(Assets::GetTileByIndex(i_tiles[(*d_walls)[index].tileindex]));
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
                glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
                int type = (*d_walls)[index].type;
                float light = 1.0F;

                if(type == 0) wline = t_line(tx,ty,tx,ty+1);
                if(type == 1) wline = t_line(tx+1,ty,tx+1,ty+1);
                if(type == 2) wline = t_line(tx,ty,tx+1,ty);
                if(type == 3) wline = t_line(tx,ty+1,tx+1,ty+1);
                if(type == 4) wline = t_line(tx,ty,tx+1,ty+1);
                if(type == 5) wline = t_line(tx+1,ty,tx,ty+1);

                if(mode && !hasFloor)
                {
                    col = !(simple_intersect(pline,wline).empty());
                    wallpiece = index;
                }

                if(col && mode)glColor3f(0.6,1.0,0.6);

                glBegin(GL_QUADS);
                    if(type == 0) // Left wall
                    {
                        glNormal3f(1,0,0);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(1,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty+1,light);
                    }
                    if(type == 1) // Right wall
                    {
                        glNormal3f(-1,0,0);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx+1,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(1,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty,light);
                    }
                    if(type == 2) // Top wall
                    {
                        glNormal3f(0,0,1);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(1,tyo); glVertex4f(tx+1,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(1,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty,light);
                    }
                    if(type == 3) // Bottom wall
                    {
                        glNormal3f(0,0,-1);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(1,tyo); glVertex4f(tx,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(1,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty+1,light);
                    }
                    if(type == 4) // Diagonal Left Wall
                    {
                        glNormal3f(1,0,1);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(txo+1.41F,tyo); glVertex4f(tx+1,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy+1,0); glTexCoord2f(txo+1.41F,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty+1,light);
                        light = GetNearestLightLevel(ix,iy,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty,light);
                    }
                    if(type == 5) // Diagonal Right Wall
                    {
                        glNormal3f(1,0,1);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,tyo); glVertex4f(tx+1,ceilheight,ty,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo+1.41F,tyo); glVertex4f(tx,ceilheight,ty+1,light);
                        light = GetNearestLightLevel(ix,iy+1,0); glTexCoord2f(txo+1.41F,(ceilheight-floorheight)/2); glVertex4f(tx,floorheight,ty+1,light);
                        light = GetNearestLightLevel(ix+1,iy,0); glTexCoord2f(txo,(ceilheight-floorheight)/2); glVertex4f(tx+1,floorheight,ty,light);
                    }
                glEnd();
            }
        }
    }

    sf::Shader::bind(NULL);

}

std::vector<T_ETile>& Map::floors()
{
    // Failsafe
    if(!d_floors) d_floors = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    return (*d_floors);
}

std::vector<T_ETile>& Map::ceils()
{
    if(!d_ceils) d_ceils = new std::vector<T_ETile>(MAP_SIZE*MAP_SIZE);
    return (*d_ceils);
}

std::vector<T_EWall>& Map::walls()
{
    if(!d_walls) d_walls = new std::vector<T_EWall>(MAP_SIZE*MAP_SIZE);
    return (*d_walls);
}

std::vector<T_EEntity>& Map::ents()
{
    if(!d_ents) d_ents = new std::vector<T_EEntity>(MAP_SIZE*MAP_SIZE);
    return (*d_ents);
}

std::vector<T_ELight>& Map::lights()
{
    if(!d_lights) d_lights = new std::vector<T_ELight>(MAP_SIZE*MAP_SIZE);
    return (*d_lights);
}

float Map::GetNearestLightLevel(float mx, float my, float mz)
{
    MapLight *nearest = NULL;
    float th = 4.0F;
    float threshold = th*th;
    float last = 0.0F;

    bool isPlayer = false;

    // Player light overrides normal lights...
    float pth = 5.0F;
    float ppt = pth*pth;

    for(int i = 0; i < mlights.size(); i++)
    {
        if(mlights[i].type == 0)continue;

        float tx = fabs(mx-mlights[i].x);
        float ty = fabs(my-mlights[i].y);
        float tz = fabs(mz-mlights[i].z);

        if((tx*tx)+(ty*ty) < threshold)
        {
            nearest = &mlights[i];
            threshold = (tx*tx)+(ty*ty);
        }
    }

    if(!nearest) return 1.0F;

    //cout << "LIGHT FOUND" << endl;
    float l = threshold/(th*th);
    if(isPlayer) l = 1.0F-(ppt/(pth*pth));
    if(l < 0.1F)l = 0.1F;
    //if(l > 0.9F)l = 0.9F;
    return l;
}

MXVector Map::GetNearestLightPos(float mx, float my, float mz)
{
    MXVector nearest(0.0,0.0,0.0);
    float th = 3.0F;
    float threshold = th*th;
    float last = 0.0F;

    for(int i = 0; i < mlights.size(); i++)
    {
        float tx = fabs(mx-mlights[i].x);
        float ty = fabs(my-mlights[i].y);
        float tz = fabs(mz-mlights[i].z);

        if((tx*tx)+(ty*ty) < threshold)
        {
            nearest = MXVector(mlights[i].x,mlights[i].y,mlights[i].z);
            threshold = (tx*tx)+(ty*ty);
        }
    }

    //cout << "LIGHT FOUND" << endl;

    return nearest;
}

void Map::setPlayerLight(float x, float y, float z, int type)
{
    // Sets the 'player' light, or light zero.
    if(mlights.size() == 0)
    {
        MapLight player_light;
        player_light.x = x;
        player_light.y = y;
        player_light.z = z;
        player_light.type = type;
        mlights.push_back(MapLight(player_light));
    }
    else
    {
        mlights.front().x = x;
        mlights.front().y = y;
        mlights.front().z = z;
        mlights.front().type = type;
    }
}


#include <input.h>
#include <math.h>
#include <config.h>

Input::Input(){}
Input::~Input(){}

bool Input::keymap[256];
bool Input::buttons[16];

void Input::init() // Should make this self-manageable
{
    for(int i = 0; i < 256; i++)
    {
        keymap[i] = false;
    }
    for(int i = 0; i < 16; i++)
    {
        buttons[i] = false;
    }
}

bool Input::keyDown(sf::Keyboard::Key key)
{
    if(sf::Keyboard::isKeyPressed(key))
    {
        keymap[key] = true;
        return true;
    }
    else
    {
        keymap[key] = false;
    }

    return false;
}

bool Input::keyPressed(sf::Keyboard::Key key)
{
    if(sf::Keyboard::isKeyPressed(key))
    {
        if(keymap[key])return false;
        keymap[key] = true;
        return true;
    }
    else
    {
        keymap[key] = false;
    }
    return false;
}

bool Input::click(sf::Mouse::Button btn)
{
    if(sf::Mouse::isButtonPressed(btn))
    {
        if(buttons[btn])return false;
        buttons[btn] = true;
        return true;
    }
    else
    {
        buttons[btn] = false;
    }

    return false;
}

bool Input::clickd(sf::Mouse::Button btn)
{
    if(sf::Mouse::isButtonPressed(btn))
    {
        buttons[btn] = true;
        return true;
    }
    else
    {
        buttons[btn] = false;
    }

    return false;
}

// Gamepad controls
float Input::deadzone = 20.0F;

float Input::axisX()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    float force = sf::Joystick::getAxisPosition(0, sf::Joystick::X);
    if(fabs(force) > deadzone) return force;

    return 0.0F;
}

float Input::axisY()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    float force = sf::Joystick::getAxisPosition(0, sf::Joystick::Y);
    if(fabs(force) > deadzone) return force;

    return 0.0F;
}

float Input::axisZ()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    float force = sf::Joystick::getAxisPosition(0, sf::Joystick::Z);
    if(fabs(force) > deadzone) return force;

    return 0.0F;
}

float Input::axisU()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    float force = sf::Joystick::getAxisPosition(0, sf::Joystick::U);
    if(fabs(force) > deadzone) return force;

    return 0.0F;
}

float Input::axisR()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    float force = sf::Joystick::getAxisPosition(0, sf::Joystick::R);
    if(fabs(force) > deadzone) return force;

    return 0.0F;
}

bool Input::buttonDown(int btn)
{
    if(sf::Joystick::isButtonPressed(0,btn))
    {
        buttons[btn] = true;
        return true;
    }

    return false;
}

bool Input::buttonPressed(int btn)
{
    if(buttons[btn])
    {
        if(!sf::Joystick::isButtonPressed(0,btn)) buttons[btn] = false;
        return false;
    }

    if(sf::Joystick::isButtonPressed(0,btn))
    {
        buttons[btn] = true;
        return true;
    }

    return false;
}

bool Input::povLeft()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    return sf::Joystick::getAxisPosition(0, sf::Joystick::PovX) < 0.0F;
}
bool Input::povRight()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    return sf::Joystick::getAxisPosition(0, sf::Joystick::PovX) > 0.0F;
}
bool Input::povUp()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    return sf::Joystick::getAxisPosition(0, sf::Joystick::PovY) < 0.0F;
}
bool Input::povDown()
{
    if(!global_config.gamepad && !sf::Joystick::isConnected(0)) return 0.0F;

    return sf::Joystick::getAxisPosition(0, sf::Joystick::PovY) > 0.0F;
}

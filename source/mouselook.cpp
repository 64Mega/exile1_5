// Simple mouselook implementation
// based on Exile's code

#include <mouselook.h>
#include <spectrum/spectrum.h>
#include <SFML/Window.hpp>
#include <SFML/System.hpp>

using namespace std;

MouseLook::MouseLook(){}

MouseLook::~MouseLook(){}

int MouseLook::mx = 400;
int MouseLook::my = 300;
float MouseLook::deltax = 0;
float MouseLook::deltay = 0;

void MouseLook::init()
{
    sf::Mouse::setPosition(sf::Vector2i(400,300));
    deltax = 0;
    deltay = 0;
    mx = 400;
    my = 400;
}

void MouseLook::update()
{
    // Transform
    mx = sf::Mouse::getPosition(Engine::GetEngine().GetRenderContext()).x;
    my = sf::Mouse::getPosition(Engine::GetEngine().GetRenderContext()).y;
    int tx = mx-320;
    int ty = my-240;

    deltax = tx/320.0F;
    deltay = ty/240.0F;

    sf::Mouse::setPosition(sf::Vector2i(320,240), Engine::GetEngine().GetRenderContext());
}

// A scripted entity.
// Can load all of it's code from .lua files, and can thus be added to the game
// via the assets file.

#ifndef ENTSCRIPTED_H
#define ENTSCRIPTED_H

#include <slb.hpp>
#include <lua/lua.h>
#include <lua/lualib.h>
#include <lua/lauxlib.h>

#include <mxmath.h>
#include <spectrum/spectrum.h>
#include <entity.h>

class ScriptedEntity : public Entity
{
private:
    std::string script;

    lua_State *L;

    void registerBinding(); // Registers all bound classes to the Lua state.
public:

    ScriptedEntity();
    virtual ~ScriptedEntity();

    static int GetNextID();

    void Setup(std::string script, int id);

    virtual void update();
    virtual ScriptedEntity& copy();
    virtual void spawn(float x, float y, float z, int meta);

    // API stuff
    static void PlayerResetView();
    static void EngineGo2D(bool);
    static void EngineGo3D();

};

#endif // ENTSCRIPTED_H

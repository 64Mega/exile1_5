#ifndef TILEDATA_H
#define TILEDATA_H

struct T_ETile
{
    T_ETile();

    int meta;
    int tileindex;
    int height;
};

struct T_EWall
{
    T_EWall();
    int meta;
    int type;
    int tileindex;
};

struct T_EEntity
{
    T_EEntity();
    int meta;
    int type;
    int height;
};

struct T_ELight
{
    T_ELight();
    int level;
};

#endif // TILEDATA_H

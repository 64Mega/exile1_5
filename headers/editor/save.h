// Requests a filename and then calls parent->save(filename)

#ifndef SAVESCREEN_H
#define SAVESCREEN_H

#include <screen.h>
#include <screens/editor.h>

class SaveScreen : public Screen
{
protected:
    ScreenEditor* parent;
public:
    SaveScreen();
    SaveScreen(ScreenEditor* p);
    ~SaveScreen();

    void update();
};

#endif // SAVESCREEN_H

// Loads a map

#ifndef EDITOR_LOAD_H
#define EDITOR_LOAD_H

#include <screen.h>
#include <screens/editor.h>

class LoadScreen : public Screen
{
    protected:
    ScreenEditor* parent;
public:
    LoadScreen();
    LoadScreen(ScreenEditor* p);
    ~LoadScreen();

    void update();
};

#endif // EDITOR_LOAD_H

// Tile selector screen.

#ifndef TILESWATCH_H
#define TILESWATCH_H

#include <screen.h>
#include <screens/editor.h>
#include <SFML/Graphics.hpp>

class TileSwatch : public Screen
{
protected:
    ScreenEditor* parent;
    sf::Texture tex_cursor;
public:
    TileSwatch();
    TileSwatch(ScreenEditor* editor);
    ~TileSwatch();

    void update();
};

#endif // TILESWATCH_H

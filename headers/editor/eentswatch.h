
// Tile selector screen.

#ifndef EENTSWATCH_H // DERP
#define EENTSWATCH_H

#include <screen.h>
#include <screens/editor.h>
#include <SFML/Graphics.hpp>


class EEntSwatch : public Screen
{
protected:
    ScreenEditor* parent;
    sf::Texture tex_cursor;
public:
    EEntSwatch();
    EEntSwatch(ScreenEditor* editor);
    ~EEntSwatch();

    void update();
};

#endif // EENTSWATCH_H

// Screen that displays the message:
// "Save before exiting? (Y/N)"
// Before leaving.
// --

#ifndef EDITOR_EXIT_H
#define EDITOR_EXIT_H

#include <screen.h>
#include <screens/editor.h>

class ExitScreen : public Screen
{
protected:
    ScreenEditor* parent;
    ExitScreen();
public:
    ExitScreen(ScreenEditor* p);
    ~ExitScreen();

    void update();
};

#endif // EDITOR_EXIT_H

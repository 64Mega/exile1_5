// The 3D side of the editor, for editing heights, maybe placing decals, changing textures, etc.

#ifndef EDIT3D_H
#define EDIT3D_H

#include <screen.h>
#include <screens/editor.h>
#include <player.h>
#include <map.h>

#define MOUSE_STRENGTH 16

class Edit3D : public Screen
{
protected:
    ScreenEditor* parent;
    float x, y, z;
    float rx, ry;
    Map pmap;
    Player pplayer;
    bool shaders;
public:
    Edit3D(ScreenEditor* p);
    ~Edit3D();

    void update();
};

#endif // EDIT3D_H

// Updated billboard code.
//

#ifndef NBILLBOARD_H
#define NBILLBOARD_H

#include <spectrum/engine/assets.h>
#include <SFML/Graphics.hpp>
#include <gl/gl.h>

class NBillboard 
{
	sf::Texture *img_billboard;
	public:
		NBillboard();
		~NBillboard();

		void load(std::string imgname);
		void render(float x, float y, float z);
};

#endif

// The Player class
// --

#ifndef PLAYER_H
#define PLAYER_H

#include <spectrum/spectrum.h>
#include <mxmath.h>

#include <entity.h>

#define PI 3.1425
#define DEGTORADS(x) (x*0.0174532)

class Player : public Entity
{
private:
    static Player* c_player;
    float boboffset;
    float bobtheta;
    int lightsource;
public:
    float crouch;
    float pvx, pvy, pvz;

    Player();
    Player(const Player& p);
    Player(MXVector pos, float rotation);

    virtual ~Player();

    virtual void update();
    virtual Player& copy();
    virtual void spawn(float x, float y, float z, int meta);
    static Player* getPlayer();

    void setCustomView(float rx, float ry);
    void resetView();

    void setYRot(float ry);
};

#endif // PLAYER_H

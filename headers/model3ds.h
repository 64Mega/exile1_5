// Uses Lib3ds to load 3DS models

#ifndef MODEL3DS_H
#define MODEL3DS_H

#include <lib3ds.h>
#include <string>
#include <gl/gl.h>

class Model3DS
{
public:
    Model3DS(std::string fname);
    virtual void draw() const;
    virtual void createVBO();
    virtual ~Model3DS();

    float framespeed;
    virtual void cycle();
protected:
    float f_frame;
    int c_frame;
    int f_begin;
    int f_end;
    void getFaces();
    unsigned int total_faces;
    Lib3dsFile* model;
    GLuint vertexVBO, normalVBO, textureVBO;
};
#endif // MODEL3DS_H

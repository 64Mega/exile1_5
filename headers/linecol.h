// Line/Line collision methods

#ifndef LINECOL_H
#define LINECOL_H

#include <vector>

struct t_line
{
    float ax, ay;
    float bx, by;

    t_line(float x1, float y1, float x2, float y2);
};

struct t_point
{
    float x, y;

    t_point(float ax, float ay);
};

std::vector<float> overlap_intervals(float ub1, float ub2);
std::vector<t_point> simple_intersect(t_line a, t_line b);
std::vector<t_point> line_oned_intersect(t_line a, t_line b);

#endif // LINECOL_H

// A Sequence of Textures
// Used for animated sprites
// --
// Several features:
//   - Loads from a .seq file.
//   - Three loop modes (No Loop, Forward, Ping-Pong)
//   - isEnd() method to check if animation has reached the end
//   - animate() method to 'cycle' the animation

#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

#include <MIO.h>

enum looptype
{
    LOOP_NONE = 0,
    LOOP_FORWARD,
    LOOP_PINGPONG
};

class Sequence
{
public:
    Sequence(std::string filename);
    ~Sequence();

    int animate(); // Returns current frame
    int getCurrentFrame(); // As on the tin
    int getNumFrames();

    bool isEnd();
    bool isBeginning();

    void setLoopType(looptype type);
    void setSpeed(float speed);
    void setFrame(int frame);

    sf::Texture* getTexture(); // Returns the texture for the current frame

protected:
    std::vector<sf::Texture*> textures;
    int c_frame;
    int num_frames;
    float framespeed;
    float f_frame;

    looptype ltype;

    void loadIntoTextures(std::string seqFileName);
};

#endif // SEQUENCE_H

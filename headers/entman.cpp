
#include <entman.h>
#include <map.h>

EntMan* EntMan::current = NULL;

EntMan::EntMan()
{
    EntMan::current = this;
}

EntMan::~EntMan()
{
    for(entitr i = entlist.begin(); i != entlist.end(); i++)
    {
        Entity* dptr = (*i);
        i = entlist.erase(i);
        delete dptr;
        dptr = NULL;
        if(i == entlist.end()) break;
    }
}

// Spawn function. Manually spawns in a new object, assuming it is set to correct positions etc.
void EntMan::spawn(Entity* newent)
{
    entlist.push_back(newent);
}

// Spawns all entities in current map's entlist, by ID. Requires Protos to be set up correctly.
void EntMan::mapSpawn()
{
    // Get all entities from map and spawn. Ignore out-of-range errors.
    Map* map = Map::getMap();
    if(map == NULL)
    {
        cout << "No map to spawn entities in!" << endl;
        return;
    }
    if(map->map_loaded == false)
    {
		cout << "Map not loaded! Can't spawn on empty map!" << endl;
		return;
    }

    // Clear entities out.

    for(entitr i = entlist.begin(); i != entlist.end(); i++)
    {
        Entity* dptr = (*i);
        i = entlist.erase(i);
        delete dptr;
        dptr = NULL;
        if(i == entlist.end()) break;
    }

    typedef vector<T_EEntity>::iterator mit;

    int posindex = 0;
    for(mit i = map->ents().begin(); i != map->ents().end(); i++)
    {
        int mid = (*i).type;
        int posx = posindex%MAP_SIZE;
        int posy = posindex/MAP_SIZE;
        int meta = (*i).meta;
        int height = (*i).height;
        posindex++;
        if(mid < 0)continue;



        // Go through proto list and find a match for the current entity id.
        for(entitr k = entProtos.begin(); k != entProtos.end(); k++)
        {
            if((*k)->ID() == mid)
            {
                // Most arcane line of code in my engine so far.
                // Uses the Entity::copy() virtual method to return a copy of
                // whatever the entity actually is, as a reference, then
                // takes the address of that. Looks weird because of the
                // iterator dereference.
		// ~~
		// Future Grizzled Me: "Nope. It gets far worse."
                Entity* temp = &(*k)->copy();
                cout << "SPAWNING A THING" << endl;
                cout << "POSITION: (" << posx << "," << posy << ")" << endl;

                temp->spawn(posx,height,posy,meta);
                entlist.push_back(temp);
                break;
            }
        }
    }
}

// Update each entity in the list
void EntMan::update()
{
    for(entitr i = entlist.begin(); i != entlist.end(); i++)
    {
        (*i)->update();

        if((*i)->dead)
        {
            Entity* dptr = (*i);
            i = entlist.erase(i);
            delete dptr;
            dptr = NULL;
            if(i == entlist.end()) break;
        }
    }
}

// Return list of ents
std::vector<Entity*>& EntMan::ents()
{
    return entlist;
}

// List of entity protos
std::vector<Entity*> EntMan::entProtos;

// Add a proto to the proto list
static void EntMan::addProto(Entity* proto)
{
    entProtos.push_back(proto);
}

// Destroy all protos
static void EntMan::clearProtos()
{
    for(entitr i = entProtos.begin(); i != entProtos.end(); i++)
    {
        Entity* dptr = (*i);
        i = entProtos.erase(i);
        delete dptr;
        dptr = NULL;
        if(i == entProtos.end()) break;
    }
}

// Get current instance of EntMan
static EntMan* EntMan::getCurrent()
{
    return current;
}



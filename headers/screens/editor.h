// The editor.
// Self contained screen, with all the required functionality.

#ifndef EDITOR_H
#define EDITOR_H

#include <screen.h>
#include <SFML/Graphics.hpp>

#include <list>
#include <editor/tiledata.h>

#define MAP_SIZE 128

enum EditModes
{
    Mode_Select = 0,
    Mode_Floor,
    Mode_Ceiling,
    Mode_Wall,
    Mode_Ents,
    Mode_Lights
};

class ScreenEditor : public Screen
{
private:
    void drawTile(int mx, int my);
    void drawWall(int mx, int my);
    void drawEnts(int mx, int my);
    void drawLights(int mx, int my);
protected:
    sf::Texture tex_cursor;
    int view_x;
    int view_y;

    float camx, camz;
    float camr;

    bool mbLeft, mbRight;

    bool canExit;

    EditModes mode;

    int wallmode;

    unsigned int c_tile;
    unsigned int c_eent;

    std::list<Screen*> screenlist;

    // Map data
    std::vector<T_ETile>* d_floors;
    std::vector<T_ETile>* d_ceils;
    std::vector<T_EWall>* d_walls;
    std::vector<T_EEntity>* d_ents;
    std::vector<T_ELight>* d_lights;

public:
    ScreenEditor();
    ~ScreenEditor();

    void update();

    void Clear();

    void save(std::string filename); // Saves the current map to a file.
    void load(std::string filename); // Loads a map

    friend class TileSwatch;
    friend class EEntSwatch;
    friend class Edit3D;
    friend class ExitScreen;

    std::string mapname;

    std::vector<T_ETile>& floors();
    std::vector<T_ETile>& ceils();
    std::vector<T_EWall>& walls();
    std::vector<T_EEntity>& ents();
    std::vector<T_ELight>& lights();

	// Tile index
	std::vector<std::string> t_tiles;
	std::vector<int> i_tiles;
};

#endif // EDITOR_H

// Just a copy of sorts of the basic map loading interface used in the editor, allowing the user to type in a name such as "MAP1", which
// will load the corresponding map in the maps directory.

#ifndef MAPLOAD_SCREEN_H
#define MAPLOAD_SCREEN_H

#include <screen.h>
#include <screens/mainmenu.h>

class MapLoadScreen : public Screen
{
    protected:
        ScreenMainMenu* parent;

    public:
        MapLoadScreen();
        MapLoadScreen(ScreenMainMenu* p);
        virtual ~MapLoadScreen();

        void update();
};

#endif // MAPLOAD_SCREEN_H

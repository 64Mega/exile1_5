// Static class for handling mouselook
// --

#ifndef MOUSELOOK_H
#define MOUSELOOK_H

class MouseLook
{
private:
    MouseLook();
    ~MouseLook();
public:
    static int mx;
    static int my;
    static float deltax;
    static float deltay;

    static void init();
    static void update();
};

#endif // MOUSELOOK_H

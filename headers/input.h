// Basic input wrapper
// --
// Provides two methods: keyPressed and keyDown.
// Use with sf::Keyboard::codes

#ifndef INPUT_H
#define INPUT_H

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

class Input
{
protected:
    static bool keymap[256];
    static bool buttons[16];
    Input();
public:
    ~Input();
    static void init();
    static bool keyPressed(sf::Keyboard::Key key);
    static bool keyDown(sf::Keyboard::Key key);
    static bool click(sf::Mouse::Button btn);
    static bool clickd(sf::Mouse::Button btn);

    // Gamepad controls
    static float axisX();
    static float axisY();
    static float axisZ();
    static float axisU();
    static float axisR();

    static bool buttonDown(int btn);
    static bool buttonPressed(int btn);

    static bool povLeft();
    static bool povRight();
    static bool povUp();
    static bool povDown();

    static float deadzone; // Controller deadzone
};

#endif // INPUT_H

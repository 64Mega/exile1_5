// Map loader
// --
// Loads files created with the internal .MAP format

#ifndef MAP_H
#define MAP_H

#include <screens/editor.h> // For the MAP_SIZE constant
#include <editor/tiledata.h>
#include <spectrum/spectrum.h>
#include <mxmath.h>

struct MapLight
{
    float x, y, z;
    int type;
};

class Map
{
protected:
    static Map* c_map;

    sf::Shader mapshader;

    std::vector<T_ETile>* d_floors;
    std::vector<T_ETile>* d_ceils;
    std::vector<T_EWall>* d_walls;
    std::vector<T_EEntity>* d_ents;
    std::vector<T_ELight>* d_lights;

    std::vector<MapLight> mlights;

public:
    Map();
    ~Map();

    bool map_loaded;

    void clear();

    string name;

    static Map* getMap();

    MXVector getPlayerStart();

    void setPlayerLight(float x, float y, float z, int type = 0);
    float GetNearestLightLevel(float mx, float my, float mz);
    MXVector GetNearestLightPos(float mx, float my, float mz);

    void load(string filename);
    void render(int posx, int posy, bool mode = false, bool shaders = false);

    bool cellCollision(float x1, float y1, float x2, float y2);

    std::vector<T_ETile>& floors();
    std::vector<T_ETile>& ceils();
    std::vector<T_EWall>& walls();
    std::vector<T_EEntity>& ents();
    std::vector<T_ELight>& lights();

    // Index vars for the 3D editor
    int floorpiece;
    int wallpiece;
    int ceilpiece;

	// These vars store the tile table
	std::vector<std::string> t_tiles;
	std::vector<int> i_tiles;
};

#endif // MAP_H


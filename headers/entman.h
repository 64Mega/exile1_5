// Entity Manager
// Handles a list of entities, removes dead entities, etc.
// --

#ifndef ENTMAN_H
#define ENTMAN_H

#include <spectrum/spectrum.h>
#include <entity.h>

typedef std::vector<Entity*>::iterator entitr;

class EntMan
{
protected:
    static EntMan* current;
    std::vector<Entity*> entlist;
public:
    EntMan();
    ~EntMan();

    void spawn(Entity* newent);
    void mapSpawn(); // Automatically spawns all entities in the current map.

    void update(); // Run each entity's update method.

    std::vector<Entity*>& ents();

    static std::vector<Entity*> entProtos;
    static void addProto(Entity* proto);
    static void clearProtos();
    static EntMan* getCurrent();
};
#endif // ENTMAN_H

// Loads and renders a Wavefront OBJ model exported from Blender.
// Includes texture coordinates, texture loading and normals.
// --
// Only compatible with Triangulated meshes.
// --
// This is a 'static' obj.
// Use MOB type models for animated obj models.

#ifndef MODELOBJ_H
#define MODELOBJ_H

#include <string>
#include <regex>
#include <gl/glew.h>
#include <gl/gl.h>

/// TODO: Add VBO support again

struct t_vert
{
    float x, y, z;
}vert;


class ModelObj
{
protected:
    std::string texture;
    t_vert *verts;
    GLuint vbo_verts;
    GLuint vbo_norms;
    GLuint vbo_texco;

    void buildVBO();
public:
    ModelObj();
    ~ModelObj();

    void loadFromFile(std::string filename, std::string tex);
    void render();
};
#endif // MODELOBJ_H

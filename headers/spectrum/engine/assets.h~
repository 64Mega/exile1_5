// Defines the Assets class, which is designed to be used in a static context
// It prevents the common mistake of re-loading resources each time an
// object is created

#ifndef ASSETS_H
#define ASSETS_H

#include "emustream.h"
#include <mxmath.h>

#include "mpk2.h"


/**
    Loads and manages various assets used by the game.
    Currently handles:
        sf::Image
        GameObject* (Instantiation templates, for Mapcode use)
    @author Mega
*/

class Object;
class ScriptedEntity;
class GameObject;
class Assets
{
    public:
        Assets();
        ~Assets();

        static void Initialize();
        static void Destroy();

        static void LoadAssets();

        static sf::Texture* GetImage(std::string name);
        static sf::Texture* GetImageByIndex(int index);

        static sf::Texture* GetTile(std::string name);
        static sf::Texture* GetTileByIndex(int index);
        static int GetNumTiles();
        static std::string GetTileName(int index);
		static std::string GetTileNumber(std::string name);

        static sf::Texture* GetNMap(std::string name);
        static sf::Texture* GetNMapByIndex(int index);

        static sf::Texture* GetEEnt(std::string name);
        static sf::Texture* GetEEntByIndex(int index);
        static int GetNumEEnts();
        static std::string GetEEntName(int index);

        static sf::Texture* GetDecal(std::string name);
        static sf::Texture* GetDecalByIndex(int index);

        static void AddImage(std::string filename, std::string ident);
        static void AddImageX(std::string filename, std::string ident);

        static void AddTile(std::string filename, std::string ident);
        static void AddTileX(std::string filename, std::string ident);

        static void AddNMap(std::string filename, std::string ident);
        static void AddNMapX(std::string filename, std::string ident);

        static void AddDecal(std::string filename, std::string ident);
        static void AddDecalX(std::string filename, std::string ident);

        static void AddEEnt(std::string filename, std::string ident);

        static int AddObject(GameObject* obj);
        static GameObject* Instantiate(int index, int x, int y);
        static std::string GetLevel(int idex);
        static void AddLevel(std::string lname);

        static void AddScriptedEnt(int id, std::string script);

        static void AddMusic(std::string mname, std::string iname);
        static void AddSound(std::string sname, std::string iname);

        static void PlaySound(int index);
        static void PlaySound(std::string name);
        static void PlaySound3D(int index, MXVector pos); // Plays a faked positional sound.
        static void PlaySound3D(std::string name, MXVector pos); // Plays a faked positional sound.
        static void PlayMusic(int index);
        static void PlayMusic(std::string name);
        static sf::Sound* Assets::GetSound(std::string name);

        static void SetMusicVolume(int vol);
        static void SetSoundVolume(int vol);

        static void LoadList(std::string fname);
        static void LoadPack(std::string fname);

        static void UnlockBonusFiles();

        static int TileWidth;
        static int TileHeight;

        static int svolume, mvolume;
        static bool music, sound;

    private:
        static vector<sf::Texture*> ls_images;
        static vector<sf::Texture*> ls_tiles;
        static vector<sf::Texture*> ls_nmaps;
        static vector<sf::Texture*> ls_eents;
        static vector<sf::Texture*> ls_decals;
        static vector<GameObject*> ls_objects;
        static vector<ScriptedEntity*> ls_sc_ents;

        static vector<sf::SoundBuffer*> ls_sbuffers;
        static vector<sf::Sound*> ls_sounds;

        // Name tables
        static vector<std::string> nt_images;
        static vector<std::string> nt_tiles;
        static vector<std::string> nt_nmaps;
        static vector<std::string> nt_eents;
        static vector<std::string> nt_decals;
        static vector<std::string> nt_levels; // Level paths. Used for in-game switching.
        static vector<std::string> nt_music;
        static vector<std::string> nt_musici;
        static vector<std::string> nt_sounds;

        static emuStream *myEmuStream;

        static MPK2 packfile;
        // No nametable for objects. They use indices instead
};

#endif

// Defines an Entity
// --
// Uses a design structure whereby all currently existing entities are
// tracked by the Entity class, and static methods are provided to
// iterate through and manage them.

#ifndef ENTITY_H
#define ENTITY_H

#include <vector>
#include <mxmath.h>
#include <SFML/graphics.hpp>

class Entity
{
protected:
    int id;
public:
    static int g_id;
    MXVector pos;
    MXVector rot;

    bool dead;

    Entity();
    Entity(MXVector apos);
    Entity(const Entity& ent); // Copy constructor. Important for protos. Define for each subclass.
    virtual ~Entity();

    sf::Texture* getIcon();

    Entity& setPos(MXVector apos);
    Entity& setRot(MXVector arot);

    virtual void update();

    int ID();
    int meta;

    virtual Entity& copy(); // Returns a copy of the instance.

    virtual void spawn(float x, float y, float z, int meta); // Supposed to handle spawning related things.
};

#endif // ENTITY_H

// Mega's GL utilities
// Mostly billboarding stuff
//
// Should work independently with the usual Core OpenGL headers

#ifndef MGL_UTIL_H
#define MGL_UTIL_H

void mglBillboardBegin(); // ModelViewMatrix based billboard technique.
void mglBillboardEnd();

void mglDrawTestQuad(float x, float y, float z, float r = 1.0F);

#endif // MGL_UTIL_H

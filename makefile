WORKDIR=%cd%

CC=gcc
CXX=g++
AR=ar
LD=g++
WINDRES=windres

INC_DEBUG=-Iheaders/ -Iapi/ -IC:/Dev/myinclude -IC:/Dev/SFML-2.0/include -IC:/MingW-4.7/include
CFLAGS_DEBUG=-std=c++11 -w -pg -g -Wall -fpermissive -DSFML_STATIC
RESINC_DEBUG=
RCFLAGS_DEBUG=
LIBDIR_DEBUG=-Llibs/ -LC:/Dev/myinclude/lib -LC:/Dev/SFML-2.0/lib -LC:/MingW-4.7/lib
LDFLAGS_DEBUG=-pg -lgmon -l3ds-1_3 -llua -lLuaSLB -lGME-win32-d -lglu32 -lsfml-audio-s -lsfml-graphics-s -lsfml-window-s -lsfml-system-s -lsfml-main -lglew32.dll -lglew32 -lglew_static -lgcc_eh -static-libgcc -static-libstdc++
OBJDIR_DEBUG=.\obj\debug
OBJDIR_WINDEBUG=.\obj\debug
DEP_DEBUG=
OUT_DEBUG=./ExileDebug.exe

OBJ_DEBUG=$(OBJDIR_DEBUG)\\source\\map.o $(OBJDIR_DEBUG)\\source\\main.o $(OBJDIR_DEBUG)\\source\\linecol.o $(OBJDIR_DEBUG)\\source\\input.o $(OBJDIR_DEBUG)\\source\\entity.o $(OBJDIR_DEBUG)\\source\\entities\\EntScripted.o $(OBJDIR_DEBUG)\\source\\editor\\tileswatch.o $(OBJDIR_DEBUG)\\source\\editor\\tiledata.o $(OBJDIR_DEBUG)\\source\\editor\\save.o $(OBJDIR_DEBUG)\\source\\editor\\load.o $(OBJDIR_DEBUG)\\source\\editor\\exitscreen.o $(OBJDIR_DEBUG)\\source\\editor\\eentswatch.o $(OBJDIR_DEBUG)\\source\\config.o $(OBJDIR_DEBUG)\\source\\ModelObj.o $(OBJDIR_DEBUG)\\source\\MPK2.o $(OBJDIR_DEBUG)\\source\\EmuStream.o $(OBJDIR_DEBUG)\\icon.o $(OBJDIR_DEBUG)\\source\\spectrum\\object.o $(OBJDIR_DEBUG)\\source\\spectrum\\exception.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\tileset.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\sprite.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\room.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\rect.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\mxlmap.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\gameobject.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\engine.o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\assets.o $(OBJDIR_DEBUG)\\source\\sequence.o $(OBJDIR_DEBUG)\\source\\screens\\options.o $(OBJDIR_DEBUG)\\source\\screens\\mapload.o $(OBJDIR_DEBUG)\\source\\screens\\mainmenu.o $(OBJDIR_DEBUG)\\source\\screens\\gamemenu.o $(OBJDIR_DEBUG)\\source\\screens\\editor.o $(OBJDIR_DEBUG)\\source\\screens\\edit3d.o $(OBJDIR_DEBUG)\\source\\screen.o $(OBJDIR_DEBUG)\\source\\player.o $(OBJDIR_DEBUG)\\source\\mxmath.o $(OBJDIR_DEBUG)\\source\\mouselook.o $(OBJDIR_DEBUG)\\source\\model3ds.o $(OBJDIR_DEBUG)\\source\\mio.o $(OBJDIR_DEBUG)\\source\\mgl_util.o $(OBJDIR_DEBUG)\\headers\\entman.o $(OBJDIR_WINDEBUG)\api\gl_api.o $(OBJDIR_DEBUG)/api/engine_api.o $(OBJDIR_DEBUG)/api/sfml_api.o $(OBJDIR_DEBUG)\n_billboard.o

clean:
	cmd /c if exist $(OBJDIR_DEBUG) rd

before_debug: 
	cmd /c if not exist $(OBJDIR_DEBUG)\source md $(OBJDIR_DEBUG)\source
	cmd /c if not exist $(OBJDIR_DEBUG)\source\entities md $(OBJDIR_DEBUG)\source\entities
	cmd /c if not exist $(OBJDIR_DEBUG)\source\editor md $(OBJDIR_DEBUG)\source\editor
	cmd /c if not exist $(OBJDIR_DEBUG) md $(OBJDIR_DEBUG)
	cmd /c if not exist $(OBJDIR_DEBUG)\api md $(OBJDIR_DEBUG)\api
	cmd /c if not exist $(OBJDIR_DEBUG)\source\spectrum md $(OBJDIR_DEBUG)\source\spectrum
	cmd /c if not exist $(OBJDIR_DEBUG)\source\spectrum\engine md $(OBJDIR_DEBUG)\source\spectrum\engine
	cmd /c if not exist $(OBJDIR_DEBUG)\source\screens md $(OBJDIR_DEBUG)\source\screens
	cmd /c if not exist $(OBJDIR_DEBUG)\headers md $(OBJDIR_DEBUG)\\headers

debug: before_debug $(OBJ_DEBUG) 
	$(LD) $(LIBDIR_DEBUG) -o $(OUT_DEBUG) $(OBJ_DEBUG) $(LDFLAGS_DEBUG) $(LIB_DEBUG) 

$(OBJDIR_DEBUG)\\source\\map.o: source\\map.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\map.cpp -o $(OBJDIR_DEBUG)\\source\\map.o

$(OBJDIR_DEBUG)\\source\\main.o: source\\main.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\main.cpp -o $(OBJDIR_DEBUG)\\source\\main.o

$(OBJDIR_DEBUG)\\source\\linecol.o: source\\linecol.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\linecol.cpp -o $(OBJDIR_DEBUG)\\source\\linecol.o

$(OBJDIR_DEBUG)\\source\\input.o: source\\input.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\input.cpp -o $(OBJDIR_DEBUG)\\source\\input.o

$(OBJDIR_DEBUG)\\source\\entity.o: source\\entity.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\entity.cpp -o $(OBJDIR_DEBUG)\\source\\entity.o

$(OBJDIR_DEBUG)\\source\\entities\\EntScripted.o: source\\entities\\EntScripted.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\entities\\EntScripted.cpp -o $(OBJDIR_DEBUG)\\source\\entities\\EntScripted.o

$(OBJDIR_DEBUG)\\source\\editor\\tileswatch.o: source\\editor\\tileswatch.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\tileswatch.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\tileswatch.o

$(OBJDIR_DEBUG)\\source\\editor\\tiledata.o: source\\editor\\tiledata.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\tiledata.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\tiledata.o

$(OBJDIR_DEBUG)\\source\\editor\\save.o: source\\editor\\save.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\save.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\save.o

$(OBJDIR_DEBUG)\\source\\editor\\load.o: source\\editor\\load.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\load.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\load.o

$(OBJDIR_DEBUG)\\source\\editor\\exitscreen.o: source\\editor\\exitscreen.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\exitscreen.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\exitscreen.o

$(OBJDIR_DEBUG)\\source\\editor\\eentswatch.o: source\\editor\\eentswatch.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\editor\\eentswatch.cpp -o $(OBJDIR_DEBUG)\\source\\editor\\eentswatch.o

$(OBJDIR_DEBUG)\\source\\config.o: source\\config.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\config.cpp -o $(OBJDIR_DEBUG)\\source\\config.o

$(OBJDIR_DEBUG)\\source\\ModelObj.o: source\\ModelObj.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\ModelObj.cpp -o $(OBJDIR_DEBUG)\\source\\ModelObj.o

$(OBJDIR_DEBUG)\\source\\MPK2.o: source\\MPK2.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\MPK2.cpp -o $(OBJDIR_DEBUG)\\source\\MPK2.o

$(OBJDIR_DEBUG)\\source\\EmuStream.o: source\\EmuStream.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\EmuStream.cpp -o $(OBJDIR_DEBUG)\\source\\EmuStream.o

$(OBJDIR_DEBUG)\\icon.o: icon.rc
	$(WINDRES) -i icon.rc -J rc -o $(OBJDIR_DEBUG)\\icon.o -O coff $(INC_DEBUG)

$(OBJDIR_DEBUG)\\source\\spectrum\\object.o: source\\spectrum\\object.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\object.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\object.o

$(OBJDIR_DEBUG)\\source\\spectrum\\exception.o: source\\spectrum\\exception.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\exception.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\exception.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\tileset.o: source\\spectrum\\engine\\tileset.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\tileset.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\tileset.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\sprite.o: source\\spectrum\\engine\\sprite.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\sprite.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\sprite.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\room.o: source\\spectrum\\engine\\room.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\room.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\room.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\rect.o: source\\spectrum\\engine\\rect.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\rect.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\rect.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\mxlmap.o: source\\spectrum\\engine\\mxlmap.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\mxlmap.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\mxlmap.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\gameobject.o: source\\spectrum\\engine\\gameobject.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\gameobject.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\gameobject.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\engine.o: source\\spectrum\\engine\\engine.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\engine.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\engine.o

$(OBJDIR_DEBUG)\\source\\spectrum\\engine\\assets.o: source\\spectrum\\engine\\assets.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\spectrum\\engine\\assets.cpp -o $(OBJDIR_DEBUG)\\source\\spectrum\\engine\\assets.o

$(OBJDIR_DEBUG)\\source\\sequence.o: source\\sequence.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\sequence.cpp -o $(OBJDIR_DEBUG)\\source\\sequence.o

$(OBJDIR_DEBUG)\\source\\screens\\options.o: source\\screens\\options.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\options.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\options.o

$(OBJDIR_DEBUG)\\source\\screens\\mapload.o: source\\screens\\mapload.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\mapload.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\mapload.o

$(OBJDIR_DEBUG)\\source\\screens\\mainmenu.o: source\\screens\\mainmenu.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\mainmenu.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\mainmenu.o

$(OBJDIR_DEBUG)\\source\\screens\\gamemenu.o: source\\screens\\gamemenu.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\gamemenu.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\gamemenu.o

$(OBJDIR_DEBUG)\\source\\screens\\editor.o: source\\screens\\editor.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\editor.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\editor.o

$(OBJDIR_DEBUG)\\source\\screens\\edit3d.o: source\\screens\\edit3d.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screens\\edit3d.cpp -o $(OBJDIR_DEBUG)\\source\\screens\\edit3d.o

$(OBJDIR_DEBUG)\\source\\screen.o: source\\screen.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\screen.cpp -o $(OBJDIR_DEBUG)\\source\\screen.o

$(OBJDIR_DEBUG)\\source\\player.o: source\\player.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\player.cpp -o $(OBJDIR_DEBUG)\\source\\player.o

$(OBJDIR_DEBUG)\\source\\mxmath.o: source\\mxmath.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\mxmath.cpp -o $(OBJDIR_DEBUG)\\source\\mxmath.o

$(OBJDIR_DEBUG)\\source\\mouselook.o: source\\mouselook.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\mouselook.cpp -o $(OBJDIR_DEBUG)\\source\\mouselook.o

$(OBJDIR_DEBUG)\\source\\model3ds.o: source\\model3ds.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\model3ds.cpp -o $(OBJDIR_DEBUG)\\source\\model3ds.o

$(OBJDIR_DEBUG)\\source\\mio.o: source\\mio.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\mio.cpp -o $(OBJDIR_DEBUG)\\source\\mio.o

$(OBJDIR_DEBUG)\\source\\mgl_util.o: source\\mgl_util.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\mgl_util.cpp -o $(OBJDIR_DEBUG)\\source\\mgl_util.o

$(OBJDIR_DEBUG)\\headers\\entman.o: headers\\entman.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c headers\\entman.cpp -o $(OBJDIR_DEBUG)\\headers\\entman.o

$(OBJDIR_DEBUG)\n_billboard.o: source\\n_billboard.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c source\\n_billboard.cpp -o $(OBJDIR_DEBUG)\\n_billboard.o

$(OBJDIR_WINDEBUG)\api\gl_api.o: api\gl_api.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c api\gl_api.cpp -o $(OBJDIR_WINDEBUG)\api\gl_api.o

$(OBJDIR_DEBUG)/api/sfml_api.o: api/sfml_api.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c api/sfml_api.cpp -o $(OBJDIR_DEBUG)/api/sfml_api.o

$(OBJDIR_DEBUG)/api/engine_api.o: api/engine_api.cpp
	$(CXX) $(CFLAGS_DEBUG) $(INC_DEBUG) -c api/engine_api.cpp -o $(OBJDIR_DEBUG)/api/engine_api.o


clean_debug: 
	cmd /c del /f $(OBJ_DEBUG) $(OUT_DEBUG)
	cmd /c rd .
	cmd /c rd $(OBJDIR_DEBUG)\\source
	cmd /c rd $(OBJDIR_DEBUG)\\source\\entities
	cmd /c rd $(OBJDIR_DEBUG)\\source\\editor
	cmd /c rd $(OBJDIR_DEBUG)
	cmd /c rd $(OBJDIR_DEBUG)\\source\\spectrum
	cmd /c rd $(OBJDIR_DEBUG)\\source\\spectrum\\engine
	cmd /c rd $(OBJDIR_DEBUG)\\source\\screens
	cmd /c rd $(OBJDIR_DEBUG)\\headers

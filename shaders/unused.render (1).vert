uniform vec3 pvec;
uniform float brightness;
varying vec3 lightPos;

void main()
{	

	lightPos = gl_LightPos[0] * gl_ModelViewMatrix;

	gl_FrontColor = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
}
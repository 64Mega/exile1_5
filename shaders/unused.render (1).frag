uniform sampler2D tex;

void main()
{	
	vec4 texel = texture2D(tex, gl_TexCoord[0].st);
	
	vec3 ct = texel.rgb;
	float at = texel.a;
	//gl_FragColor = vec4(ct * cf * distmult * 0.5, at * af);
	
	gl_FragColor = vec4(ct.r, ct.g, ct.b, at);
}
uniform sampler2D tex;
uniform sampler2D nmap;
varying float LightDist;
varying float LightInt;
varying vec3 LightPos;

void main()
{	
	vec4 texel = texture2D(tex, gl_TexCoord[0].st);
	
	vec3 normal = vec3(0.5,0.5,0.5); // normalize(texture2D(nmap, gl_TexCoord[0].st).rgb * 2.0 -1.0);
	float diffuse = max(dot(normal, LightPos), 1.0);
	
	vec3 ct = texel.rgb;
	float at = texel.a;
	//gl_FragColor = vec4(ct * cf * distmult * 0.75, at * af);
	vec4 col = vec4(ct.r, ct.g, ct.b, at);
	col = col * LightDist;
	col = col / (LightInt);
	vec4 col2 = gl_Color;
	
	col.rgb /= diffuse*normal.b;
	// col.rgb /= normal.r;
	
	col2.a = 1.0;
	col.a = 1.0;
	
	gl_FragColor = col * col2;
}

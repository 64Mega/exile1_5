uniform vec3 LightPosition;
uniform vec4 SkyColor;
uniform vec4 GroundColor;
varying float LightDist;
varying float LightInt;
varying vec3 LightPos;

void main()
{
	vec4 plainVert = gl_Vertex.xyzw;
	plainVert.w = 1.0;
	gl_Position = gl_ModelViewProjectionMatrix * plainVert;
	// vec4 lvec = vec4(plainVert, 1.0);
	LightInt = gl_Vertex.w;
	//vec3 ecPosition = vec3(gl_ModelViewMatrix * gl_Vertex);
	vec3 ecPosition = vec3(gl_ModelViewMatrix * plainVert);
	vec3 tnorm = normalize(gl_NormalMatrix * gl_Normal);
	vec3 lightVec = normalize(LightPosition - ecPosition);
	//vec3 lightVec = normalize(lvec.xyz - ecPosition);
	
	float costheta = dot(tnorm, lightVec);
	float a = 0.5 + 0.5 * costheta;
	
	LightDist = (lightVec.x + lightVec.y + lightVec.z) / 4.0;
	//LightDist *= 2.0;

	// LightPos = LightPosition;
	LightPos = vec3(0.0,0.0,0.0);
	
	gl_FrontColor = mix(GroundColor, SkyColor, a);
	gl_BackColor = mix(GroundColor, SkyColor, a);
	gl_TexCoord[0] = gl_MultiTexCoord0;
	//gl_Position = ftransform();
}

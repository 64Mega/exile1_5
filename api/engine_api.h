/*
	Exile 1.5
	API File
	---------
	Engine API binder. Contains a host of engine functionality,
	including textures, asset management, sound/music playback,
	map handling, etc.
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#ifndef EXILE_API_ENGINE
#define EXILE_API_ENGINE

#include <SLB.hpp>
#include <lua/lua.h>


// Include all necessary engine headers
#include <emustream.h>
#include <entman.h>
#include <linecol.h>
#include <mouselook.h>
#include <config.h>
#include <map.h>
#include <player.h>
#include <mpk2.h>
#include <input.h>
#include <mgl_util.h>
#include <screen.h>
#include <entity.h>
#include <sequence.h>
#include <spectrum/spectrum.h>

class API_ENGINE {
public:
	API_ENGINE();
	~API_ENGINE();

	static void bind();

	// Some internal bindings
	static void assets_bind_image(std::string image);
	static void assets_bind_image(int index);
	static void assets_bind_tile(std::string tile);
	static void assets_bind_tile(int index);
	static void assets_bind_decal(std::string decal);
	static void assets_bind_decal(int index);
};

#endif

/*
	Exile 1.5
	API File
	---------
	Engine API implementation
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#include <engine_api.h>

API_ENGINE::API_ENGINE(){}
API_ENGINE::~API_ENGINE(){}

void API_ENGINE::bind() {
	// First, bind all static engine functions.
	// Asset Manager:
	SLB::Class<Assets>("Game::Assets")
	.constructor()
	.set("GetImage", (sf::Texture* (*)(std::string)) &Assets::GetImage)
	.set("BindTile", (void (*)(std::string)) &API_ENGINE::assets_bind_tile)
	.set("BindTile", (void (*)(int)) &API_ENGINE::assets_bind_tile)
	.set("GetNumTiles",(int (*)()) &Assets::GetNumTiles)
	.set("BindImage", (void (*)(std::string)) &API_ENGINE::assets_bind_image)
	.set("BindImage", (void (*)(int)) &API_ENGINE::assets_bind_image)
	.set("BindDecal", (void (*)(std::string)) &API_ENGINE::assets_bind_decal)
	.set("BindDecal", (void (*)(int)) &API_ENGINE::assets_bind_decal)
	.set("PlaySound", (void (*)(int)) &Assets::PlaySound)
	.set("PlaySound", (void (*)(std::string)) &Assets::PlaySound)
	.set("PlayMusic", (void (*)(int)) &Assets::PlayMusic)
	.set("PlayMusic", (void (*)(std::string)) &Assets::PlayMusic)
	.set("SetSoundVolume", (void (*)(int)) &Assets::SetSoundVolume)
	.set("SetMusicVolume", (void (*)(int)) &Assets::SetMusicVolume);

	// Input class. Really need this one.
	SLB::Class<Input>("Game::Input")
	.set("KeyPressed", (bool (*)(sf::Keyboard::Key)) &Input::keyPressed);

	// Next, we bind each class type.
	// :	
}

void API_ENGINE::assets_bind_tile(std::string tile) {
	sf::Texture::bind(Assets::GetTile(tile));
}

void API_ENGINE::assets_bind_tile(int index) {
	sf::Texture::bind(Assets::GetTileByIndex(index));
}

void API_ENGINE::assets_bind_image(std::string image) {
	sf::Texture::bind(Assets::GetImage(image));
}

void API_ENGINE::assets_bind_image(int index) {
	sf::Texture::bind(Assets::GetImageByIndex(index));
}

void API_ENGINE::assets_bind_decal(std::string decal) {
	sf::Texture::bind(Assets::GetDecal(decal));
}

void API_ENGINE::assets_bind_decal(int index) {
	sf::Texture::bind(Assets::GetDecalByIndex(index));
}

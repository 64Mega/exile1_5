/*
	Exile 1.5
	API File
	---------
	SFML Bindings
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#include <sfml_api.h>

API_SFML::API_SFML(){}
API_SFML::~API_SFML(){}

void API_SFML::bind() {
	// Bind commonly used SFML classes
	// sf::Texture
	SLB::Class<sf::Texture>("sf::Texture")
	.constructor()
	.set("LoadFromFile",(void (sf::Texture::*)(std::string)) &sf::Texture::loadFromFile)
	.set("Bind",(void (*)(std::string)) &API_SFML::sftex_bind);

	// Bind all the necessary keyboard constants...

	SLB::Class<sf::Keyboard>("Game::Key")
	.enumValue("UP", sf::Keyboard::Up)
	.enumValue("DOWN", sf::Keyboard::Down)
	.enumValue("LEFT", sf::Keyboard::Left)
	.enumValue("RIGHT", sf::Keyboard::Right)
	
	// Top row
	.enumValue("ESCAPE", sf::Keyboard::Escape)
	.enumValue("F1", sf::Keyboard::F1)
	.enumValue("F2", sf::Keyboard::F2)
	.enumValue("F3", sf::Keyboard::F3)
	.enumValue("F4", sf::Keyboard::F4)
	.enumValue("F5", sf::Keyboard::F5)
	.enumValue("F6", sf::Keyboard::F6)
	.enumValue("F7", sf::Keyboard::F7)
	.enumValue("F8", sf::Keyboard::F8)
	.enumValue("F9", sf::Keyboard::F9)
	.enumValue("F10", sf::Keyboard::F10)
	.enumValue("F11", sf::Keyboard::F11)
	.enumValue("F12", sf::Keyboard::F12)

	// Second Row
	.enumValue("TILDE", sf::Keyboard::Tilde)
	.enumValue("NUM1", sf::Keyboard::Num1)
	.enumValue("NUM2", sf::Keyboard::Num2)
	.enumValue("NUM3", sf::Keyboard::Num3)
	.enumValue("NUM4", sf::Keyboard::Num4)
	.enumValue("NUM5", sf::Keyboard::Num5)
	.enumValue("NUM6", sf::Keyboard::Num6)
	.enumValue("NUM7", sf::Keyboard::Num7)
	.enumValue("NUM8", sf::Keyboard::Num8)
	.enumValue("NUM9", sf::Keyboard::Num9)
	.enumValue("NUM0", sf::Keyboard::Num0)
	.enumValue("DASH", sf::Keyboard::Dash)
	.enumValue("EQUAL", sf::Keyboard::Equal)
	.enumValue("BACKSLASH", sf::Keyboard::BackSlash)
	.enumValue("BACKSPACE", sf::Keyboard::BackSpace)
	
	// Third Row
	.enumValue("TAB", sf::Keyboard::Tab)
	.enumValue("Q", sf::Keyboard::Q)
	.enumValue("W", sf::Keyboard::W)
	.enumValue("E", sf::Keyboard::E)
	.enumValue("R", sf::Keyboard::R)
	.enumValue("T", sf::Keyboard::T)
	.enumValue("Y", sf::Keyboard::Y)
	.enumValue("U", sf::Keyboard::U)
	.enumValue("I", sf::Keyboard::I)
	.enumValue("O", sf::Keyboard::O)
	.enumValue("P", sf::Keyboard::P)
	.enumValue("LBRACKET", sf::Keyboard::LBracket)
	.enumValue("RBRACKET", sf::Keyboard::RBracket)

	// Fourth Row (ASDF)
	.enumValue("A", sf::Keyboard::A)
	.enumValue("S", sf::Keyboard::S)
	.enumValue("D", sf::Keyboard::D)
	.enumValue("F", sf::Keyboard::F)
	.enumValue("G", sf::Keyboard::G)
	.enumValue("H", sf::Keyboard::H)
	.enumValue("J", sf::Keyboard::J)
	.enumValue("K", sf::Keyboard::K)
	.enumValue("L", sf::Keyboard::L)
	.enumValue("SEMICOLON", sf::Keyboard::SemiColon)
	.enumValue("QUOTE", sf::Keyboard::Quote)
	.enumValue("RETURN", sf::Keyboard::Return)
	
	// Fifth Row (ZXCV)
	.enumValue("LSHIFT", sf::Keyboard::LShift)
	.enumValue("Z", sf::Keyboard::Z)
	.enumValue("X", sf::Keyboard::X)
	.enumValue("C", sf::Keyboard::C)
	.enumValue("V", sf::Keyboard::V)
	.enumValue("B", sf::Keyboard::B)
	.enumValue("N", sf::Keyboard::N)
	.enumValue("M", sf::Keyboard::M)
	.enumValue("COMMA", sf::Keyboard::Comma)
	.enumValue("PERIOD", sf::Keyboard::Period)
	.enumValue("RSHIFT", sf::Keyboard::RShift)

	// Fifth Row (Command Row)
	.enumValue("LCONTROL", sf::Keyboard::LControl)
	.enumValue("LSYSTEM", sf::Keyboard::LSystem)
	.enumValue("LALT", sf::Keyboard::LAlt)
	.enumValue("SPACE", sf::Keyboard::Space)
	.enumValue("RALT", sf::Keyboard::RAlt)
	.enumValue("RSYSTEM", sf::Keyboard::RSystem)
	.enumValue("MENU", sf::Keyboard::Menu)
	.enumValue("RCONTROL", sf::Keyboard::RControl)

	// Home cluster
	.enumValue("HOME", sf::Keyboard::Home)
	.enumValue("END", sf::Keyboard::End)
	.enumValue("PAGEUP", sf::Keyboard::PageUp)
	.enumValue("PAGEDOWN", sf::Keyboard::PageDown)
	.enumValue("INSERT", sf::Keyboard::Insert)
	.enumValue("DELETE", sf::Keyboard::Delete)

	// Num pad
	.enumValue("NUMPAD0", sf::Keyboard::Numpad0)
	.enumValue("NUMPAD1", sf::Keyboard::Numpad1)
	.enumValue("NUMPAD2", sf::Keyboard::Numpad2)
	.enumValue("NUMPAD3", sf::Keyboard::Numpad3)
	.enumValue("NUMPAD4", sf::Keyboard::Numpad4)
	.enumValue("NUMPAD5", sf::Keyboard::Numpad5)
	.enumValue("NUMPAD6", sf::Keyboard::Numpad6)
	.enumValue("NUMPAD7", sf::Keyboard::Numpad7)
	.enumValue("NUMPAD8", sf::Keyboard::Numpad8)
	.enumValue("NUMPAD9", sf::Keyboard::Numpad9)
	.enumValue("DIVIDE", sf::Keyboard::Divide)
	.enumValue("MULTIPLY", sf::Keyboard::Multiply)
	.enumValue("ADD", sf::Keyboard::Add)
	.enumValue("SUBTRACT", sf::Keyboard::Subtract)
	.enumValue("PAUSE", sf::Keyboard::Pause);


}

void API_SFML::sftex_bind(std::string assetname) {
	sf::Texture::bind(Assets::GetImage(assetname));
}

/*
	Exile 1.5
	API File
	---------
	SFML Bindings
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#ifndef EXILE_API_SFML
#define EXILE_API_SFML

#include <spectrum/spectrum.h>
#include <SLB.hpp>
#include <lua/lua.h>

class API_SFML {
	public:
	API_SFML();
	~API_SFML();

	static void bind();
	static void sftex_bind(std::string assetname);
};

#endif

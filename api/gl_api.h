/*
	Exile 1.5
	API File
	---------
	The GL API provides LUA a simple interface to commonly used
	OpenGL functionality.
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#ifndef EXILE_API_GL
#define EXILE_API_GL

#include <gl/gl.h>
#include <gl/glu.h>
#include <gl/glext.h>

#include <slb.hpp>
#include <lua/lua.h>

class API_GL {
public:
	static void bind();
	API_GL();
	~API_GL();
};

#endif

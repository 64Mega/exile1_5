/*
	Exile 1.5
	API File
	---------
	The GL API provides LUA a simple interface to commonly used
	OpenGL functionality.
	---------
	Created: 24 January 2014
	Daniel Lawrence
*/

#include <gl_api.h>

using namespace std;

void API_GL::bind() {
	SLB::Class<API_GL>("GL::GL")
	// General functions

	.set("Color3f",(void (*)(float,float,float)) glColor3f)
	.set("Color4f",(void (*)(float,float,float,float)) glColor4f)
	.set("Vertex2f",(void (*)(float,float)) glVertex2f)
	.set("Vertex2i",(void (*)(int,int)) glVertex2i)
	.set("Vertex3f",(void (*)(float,float,float)) glVertex3f)
	.set("TexCoord2f",(void (*)(float, float)) glTexCoord2f)
	.set("Begin",(void (*)(int)) glBegin)
	.set("End",(void (*)()) glEnd)
	.set("BindTexture",(void (*)(int, int)) glBindTexture)
	.set("Enable",(void (*)(int)) glEnable)
	.set("Disable",(void (*)(int)) glDisable)

	// Make important GL enums visible to LUA

	.set("QUADS",GL_QUADS)
	.set("TRIANGLES",GL_TRIANGLES)
	.set("TRIANGLE_STRIP",GL_TRIANGLE_STRIP)
	.set("LINES",GL_LINES)
	.set("LINE_STRIP",GL_LINE_STRIP)
	.set("TEXTURE_2D",GL_TEXTURE_2D)
	.set("LIGHTING",GL_LIGHTING)
	.set("DEPTH_TEST",GL_DEPTH_TEST)
	.set("ALPHA_TEST",GL_ALPHA_TEST)
	.set("COLOR_MATERIAL",GL_COLOR_MATERIAL);
}

API_GL::API_GL(){}

API_GL::~API_GL(){}

varying vec3 lightDir, normal;
uniform sampler2D tex;
varying float distmult;

void main()
{
	vec3 ct, cf;
	vec4 texel;
	float intensity, at, af;
	
	intensity = 0.5; // max(dot(lightDir, normalize(normal)),0.0);
	intensity = min(max(dot(lightDir, normalize(normal)),0.0), 0.5);
	
	cf = intensity * (gl_LightSource[0].diffuse).rgb + gl_LightSource[0].ambient.rgb;
	af = gl_FrontMaterial.diffuse.a;
	
	texel = texture2D(tex, gl_TexCoord[0].st);
	
	ct = texel.rgb;
	at = texel.a;
	gl_FragColor = vec4(ct * cf * distmult * 0.5, at * af);

	//vec4 color = texture2D(tex, gl_TexCoord[0].st);
	//color = vec4(color.xyz * distmult, color.w);
	//gl_FragColor =  color;
}
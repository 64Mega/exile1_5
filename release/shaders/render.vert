varying float distmult;
uniform vec3 pvec;
uniform float brightness;
varying vec3 lightDir, normal;

void main()
{	
	// pvec = vec3(pvec*ftransform().xyz);
	vec3 apvec = vec3(pvec*gl_NormalMatrix);
	// vec3 avvec = vec3(gl_Vertex.xyz*gl_NormalMatrix);
	
	distmult = brightness - length(vec3(pvec.xyz-gl_Vertex.xyz));
	
	normal = normalize(gl_NormalMatrix * gl_Normal);
	//lightDir = normalize(vec3(gl_LightSource[0].position));
	lightDir = normalize(pvec); // normalize(vec3(pvec));
	
	gl_FrontColor = gl_Color;
	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_Position = ftransform();
}
ABYSS DEMO - EDITOR VERSION
---------------------------

So instead of the game I wanted to make, I can only present the editor, along with a basic
'walkaround' game thingy. As in: You walk, you collide with stuff, and you can crouch under 
gaps. That's about it.

Eventually, I want to turn this game into something really engaging and fun, but for now
I'll just have to showcase the engine/editor.

Enjoy the 'game'. I'm glad I have anything at all to show for this.

CONTROLS:
---------
In game, you use the standard WASD+Mouse configuration, SHIFT to crouch and Escape to bring up 
the in-game menu.

The same goes for the Editor's 3D preview/edit mode. 

In both modes, you can hit TAB to switch between Shader based rendering or fixed pipeline rendering.
Try both out and tell my what your thoughts are.

USING THE EDITOR:
-----------------
The editor is nearly complete, and there's quite a bit to mess around with.
If you want to create a level and actually play it in the 'game' mode, you must
overwrite TEST4.MAP (Derp on my part, didn't add a level selector).

The editor has two modes: 2D edit and 3D preview/edit.
You start in 2D mode, and from here you can place tiles/walls/ceilings, as well
as position the player start or the preview camera.

There are four sub-modes to use; you switch beween these with the 1,2,3 and 4 keys.
Mode 1 is "Select" mode. You can't do much in it besides place the preview cam position
with the right mouse button.

Mode 2 is "Floor" mode. It allows you to place/delete floor tiles.

Mode 3 is "Ceiling" mode. Same as floors, but for ceilings.

Mode 4 is "wall" mode. The way the wall faces can be changed by pressing the Arrow keys,
and Delete/Page Down give you diagonal wall pieces.

In order to select different tiles, you can press F1. This brings up the Tile Swatch screen.
Click on a tile from here to be taken back to the editor.

F2 brings up the "Entity" page. There is only one at the moment, the "Player Start".

F3 switches to 3D edit mode.


3D EDITOR CONTROL AND QUIRKS
============================
Besides being able to move around in the map, you can perform further editing in 3D mode.
Pointing at the floor will make tiles turn red (Turn shaders off if you can't see the marker by pressing TAB).

Red tiles can be manipulated in various ways. Scroll the mouse wheel to elevate/drop them.
Press the Right mouse button to bring up the Tile palette, and select tiles.
Press the Left mouse button to apply the tile to the sides of elevated tiles.

The detection for the 3D editor is a bit glitched (Very glitched, actually). I didn't have the time to implement
a proper Ray/Tri intersect function, so I'm just making do with this.


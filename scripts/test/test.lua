-- Lua script to test drawing.
-- Let's load a Sprite and draw it. Because we can :D

function init()
	print("Created LUA entity!");
	SLB.using(SLB.GL);

	-- Play some music, for great justice!
	SLB.Game.Assets.SetMusicVolume(255)
	SLB.Game.Assets.PlayMusic("msc_katana");
end

function update() 
	SLB.Game.SEntity.Go2D(false)
	
	
	SLB.Game.Engine.Print(0,0,"TEST",1.0,1.0,1.0);


	-- Try drawing some things with basic GL functionality
	GL.Color4f(0.5,0.5,1.0,1.0);

	SLB.sf.Texture.Bind("door1");
	
	GL.Begin(GL.QUADS);
		GL.TexCoord2f(0.0,0.0); GL.Vertex2i(50,50);
		GL.TexCoord2f(1.0,0.0); GL.Vertex2i(100,50);
		GL.TexCoord2f(1.0,1.0); GL.Vertex2i(100,100);
		GL.TexCoord2f(0.0,1.0); GL.Vertex2i(50,100);
	GL.End();
	
	GL.BindTexture(GL.TEXTURE2D, 0);
	
	SLB.Game.SEntity.ResetView()
	SLB.Game.SEntity.Go3D()
	

	if SLB.Game.Input.KeyPressed(SLB.Game.Key.TILDE) then
		return 1;
	end

	-- Returning a non-zero integer kills the instance.
	return 0
end

function destroy()
	print("Destroyed LUA entity!")
end

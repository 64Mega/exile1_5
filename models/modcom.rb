# Compiles an OBJ model and it's material file into
# an MDL file (Not related to the Quake MDL format)
# --
# TODO: Add support for animated OBJ files
# --

class Material < Object
  def initialize(name, kdr, kdg, kdb)
    @name = name
    @diffuse_r = kdr
    @diffuse_g = kdg
    @diffuse_b = kdb
  end

  attr_accessor :name
  attr_accessor :diffuse_r, :diffuse_g, :diffuse_b
end

class Model < Object
  def initialize()
    @materials = []
    @mtllib = nil
    @objects = []
    @verts = []
    @norms = []
    @face_verts = []
    @face_norms = []
    @face_texco = []
    @texco = []
  end

  def addVert(x,y,z)
    @verts << [x,y,z]
  end

  def addNorm(x,y,z)
    @norms << [x,y,z]
  end

  def addTexco(x,y)
    @texco << [x,y]
  end

  def addVertFace(a,b,c)
    @face_verts << [a,b,c]
  end

  def addNormFace(a,b,c)
    @face_norms << [a,b,c]
  end

  def addTexcoFace(a,b,c)
    @face_texco << [a,b,c]
  end

  def faces?
    @face_verts.count
  end
end

model = Model.new

modelfile = File.open("SimpleCube.obj","r")

modelfile.each_line do |line|
  rgf = /^f (\d+)?\/(\d+)?\/(\d+)? (\d+)?\/(\d+)?\/(\d+)? (\d+)?\/(\d+)?\/(\d+)?/
  rgv = /^v (\f+) (\f+) (\f+)/
  rgvn = /^vn (\f+) (\f+) (\f+)/

  if rgf =~ line then
    puts "FACE: #{$1} #{$2} #{$3} | #{$4} #{$5} #{$6} | #{$7} #{$8} #{$9}"
  end
  if rgv =~ line then
    puts "VERT!"
  end
end

modelfile.close
